/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import javafx.embed.swing.JFXPanel;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import oviceTime.database.DbHelper;
import oviceTime.model.AttendanceModel;
import oviceTime.ui.TmScrollBar;
import oviceTime.utilities.Utilities;
import oviceTime.utilities.TableRenderer;

/**
 *
 * @author user
 */
public class SummaryPanel extends javax.swing.JPanel {
    
    private final String[] columnNames = {"S/N","Full Name", "Time-In"};
    private String [][] data;
    private DbHelper dbHelper;
    private JTable table;
    private String todayDate;
    private int todayAttendance;
    private int totalWorkers;

    /**
     * Creates new form SummaryPanel
     */
    public SummaryPanel() {
        initComponents();
         dbHelper = new DbHelper(true);
         setupCalender();
         todayDate = Utilities.todayDate();
         createDailyAttendnaceTable(todayDate);
         SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
                initFxGraph();
            }
          });
        
    }
    
    /**
     * refresh the component for new data
     */
    public void refresh(){
        createDailyAttendnaceTable(todayDate);
    }
    
    private void setupCalender(){
        
        UtilDateModel model = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        
        JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
        JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
        JFormattedTextField textField = datePicker.getJFormattedTextField();
        textField.setFont(new Font("Cambria", Font.PLAIN, 12));
        textField.setBackground(Color.white);
        textField.setBorder(new MatteBorder(0, 0, 2, 0, Color.BLACK));
        textField.setText(Utilities.todayDate());
        
     
        datePickerPanel.add(datePicker, BorderLayout.CENTER);
      
        
    }
    
    /**
     * initialize the JFXpanel and add it to the graph panel
     */
    private void initFxGraph(){
        FxGraph graph = new FxGraph();
        int yAxis[]= null;
        int xAxis[] = null;
        List<AttendanceModel> modelList = dbHelper.getDailyAttendance();
        if(modelList != null){
            xAxis = new int[modelList.size()];
            yAxis = new int[modelList.size()];
            
            for(int i = 0; i < modelList.size(); i++){
                xAxis[i] = dayFromDate(modelList.get(i).getDate());
                yAxis[i] = modelList.get(i).getDailyAttendance();
            }
        }
        graph.setGraphDetails(xAxis, yAxis);
        JFXPanel fXPanel = graph.initFxComponents();
        fXPanel.setSize(400, 350);
        graphPanel.add(fXPanel, BorderLayout.CENTER);
    }
    
    public int dayFromDate(String date){
        String[] day = date.split("-");
        return Integer.parseInt(day[2]);
    }
    
    private void setUpDisplays(){
        dateLabel.setText(todayDate);
        dailyAttendancelabel.setText(String.valueOf(todayAttendance));
        totalWorkers = dbHelper.getTotalWorkers();
        totalWorkersLabel.setText(String.valueOf(totalWorkers));
        
    }
    
    private void createDailyAttendnaceTable(String date){
        tablePanel.removeAll();
        tablePanel.revalidate();
        
            data = dbHelper.getDashboardAttendanceRecords(date);
            if(data != null && data.length > 0){
                todayAttendance = data.length;
                table = new JTable(data, columnNames);
                table.setSelectionMode(1);
                table.setRowHeight(30);      
                //table.setDefaultRenderer(Object.class, new TableRenderer());
                table.setSelectionBackground(new Color(0,191,233));
                table.setSelectionForeground(Color.black);
                table.setBackground(Color.white);
                 table.setBorder(new EmptyBorder(8, 8, 8, 8));
                 table.setGridColor(Color.white);

                table.setPreferredScrollableViewportSize(new Dimension(205,320));


                JScrollPane scrollPane = new JScrollPane(table);
                scrollPane.getVerticalScrollBar().setUI(new TmScrollBar());
                scrollPane.setBackground(new Color(240,240,240));
                scrollPane.setBorder(new EmptyBorder(8, 8, 8, 8));
                this.tablePanel.add(scrollPane, BorderLayout.CENTER);
                table.setBorder(null);
                table.getSelectionModel().addListSelectionListener(new SummaryPanel.TableSelectionListener());
                setUpDisplays();
             }else{
                    
                    JLabel label = new JLabel("No Record Found");
                    label.setOpaque(true);
                    label.setForeground(new Color(47,58,74));
                    label.setFont(new Font("Cambria", Font.PLAIN, 18));
                    label.setBorder(new EmptyBorder(50, 100, 0, 100));
                    tablePanel.add(label, BorderLayout.NORTH);
                    
            }
       
         
    }
    
    /**
     * method to switch the container
     */
    private void showCoursesPanel(){
        JPanel cardsContainer = (JPanel) this.getParent();
        CardLayout cardLayout = (CardLayout) cardsContainer.getLayout();
        cardLayout.show(cardsContainer, MainHomeForm.STAFF_DETAILS_PANEL);
        
    }
    
     /**
     * this segments listens to the click event on the table
     * selects the date equivalent of the attendance of the student selected
     * used some array tricks
     */
    
    private class TableSelectionListener implements ListSelectionListener{
        
        @Override
        public void valueChanged(ListSelectionEvent e) {
           
            int row = table.getSelectedRow();//gets the index of the selected row
            String ft[] = data[row];
            //System.out.println("the selected row is  " + ft[0].toString());
           // showCoursesPanel();
            
           
        }  
    }
    
    
    public class DateLabelFormatter extends JFormattedTextField.AbstractFormatter{
            
        private String datePattern = "yyyy-MM-dd";
        private SimpleDateFormat dateFormattter = new SimpleDateFormat(datePattern);
        
        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormattter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) throws ParseException {
            if(value != null){
                Calendar cal = (Calendar) value;
                String selDate = dateFormattter.format(cal.getTime());
                createDailyAttendnaceTable(selDate);
                return selDate;
            }
            
            return "";
        }
    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        container = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        totalWorkerspanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        totalWorkersLabel = new javax.swing.JLabel();
        DailyAttendancePanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        dateLabel = new javax.swing.JLabel();
        dailyAttendancelabel = new javax.swing.JLabel();
        lateWorkersPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        totalWorkersLabel3 = new javax.swing.JLabel();
        EarlyWorkersPanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        totalWorkersLabel1 = new javax.swing.JLabel();
        graphPanel = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        tableheaderPanel = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        datePickerPanel = new javax.swing.JPanel();
        tablePanel = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();

        setPreferredSize(new java.awt.Dimension(800, 550));
        setLayout(new java.awt.BorderLayout());

        container.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 10, 10, 1));
        container.setPreferredSize(new java.awt.Dimension(800, 500));

        jPanel1.setName(""); // NOI18N
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 70));
        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 40, 5));

        totalWorkerspanel.setPreferredSize(new java.awt.Dimension(150, 70));
        totalWorkerspanel.setLayout(new java.awt.BorderLayout());

        jLabel1.setBackground(new java.awt.Color(254, 134, 97));
        jLabel1.setFont(new java.awt.Font("Cambria Math", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(33, 63, 87));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/Workers_24px.png"))); // NOI18N
        jLabel1.setName(""); // NOI18N
        jLabel1.setOpaque(true);
        jLabel1.setPreferredSize(new java.awt.Dimension(50, 70));
        totalWorkerspanel.add(jLabel1, java.awt.BorderLayout.LINE_START);

        jPanel2.setPreferredSize(new java.awt.Dimension(100, 70));

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 10)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(254, 134, 97));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Total Workers");
        jLabel2.setToolTipText("");
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel2.setName(""); // NOI18N
        jLabel2.setOpaque(true);
        jLabel2.setPreferredSize(new java.awt.Dimension(100, 35));

        totalWorkersLabel.setBackground(new java.awt.Color(255, 255, 255));
        totalWorkersLabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        totalWorkersLabel.setForeground(new java.awt.Color(33, 63, 87));
        totalWorkersLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        totalWorkersLabel.setText("0");
        totalWorkersLabel.setToolTipText("");
        totalWorkersLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        totalWorkersLabel.setName(""); // NOI18N
        totalWorkersLabel.setOpaque(true);
        totalWorkersLabel.setPreferredSize(new java.awt.Dimension(100, 35));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(totalWorkersLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(totalWorkersLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        totalWorkerspanel.add(jPanel2, java.awt.BorderLayout.EAST);

        jPanel1.add(totalWorkerspanel);

        DailyAttendancePanel.setLayout(new java.awt.BorderLayout());

        jLabel5.setBackground(new java.awt.Color(86, 189, 222));
        jLabel5.setFont(new java.awt.Font("Cambria Math", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(33, 63, 87));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/Calendar_24px.png"))); // NOI18N
        jLabel5.setName(""); // NOI18N
        jLabel5.setOpaque(true);
        jLabel5.setPreferredSize(new java.awt.Dimension(50, 70));
        DailyAttendancePanel.add(jLabel5, java.awt.BorderLayout.CENTER);

        jPanel4.setPreferredSize(new java.awt.Dimension(100, 70));

        dateLabel.setBackground(new java.awt.Color(255, 255, 255));
        dateLabel.setFont(new java.awt.Font("Segoe UI", 1, 10)); // NOI18N
        dateLabel.setForeground(new java.awt.Color(86, 189, 222));
        dateLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        dateLabel.setText("10-02-2017");
        dateLabel.setToolTipText("");
        dateLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        dateLabel.setName(""); // NOI18N
        dateLabel.setOpaque(true);
        dateLabel.setPreferredSize(new java.awt.Dimension(100, 35));

        dailyAttendancelabel.setBackground(new java.awt.Color(255, 255, 255));
        dailyAttendancelabel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        dailyAttendancelabel.setForeground(new java.awt.Color(33, 63, 87));
        dailyAttendancelabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        dailyAttendancelabel.setText("0");
        dailyAttendancelabel.setToolTipText("");
        dailyAttendancelabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        dailyAttendancelabel.setName(""); // NOI18N
        dailyAttendancelabel.setOpaque(true);
        dailyAttendancelabel.setPreferredSize(new java.awt.Dimension(100, 35));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(dateLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(dailyAttendancelabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(dateLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(dailyAttendancelabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        DailyAttendancePanel.add(jPanel4, java.awt.BorderLayout.EAST);

        jPanel1.add(DailyAttendancePanel);

        lateWorkersPanel.setLayout(new java.awt.BorderLayout());

        jLabel7.setBackground(new java.awt.Color(177, 152, 220));
        jLabel7.setFont(new java.awt.Font("Cambria Math", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(33, 63, 87));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/late_timer.png"))); // NOI18N
        jLabel7.setName(""); // NOI18N
        jLabel7.setOpaque(true);
        jLabel7.setPreferredSize(new java.awt.Dimension(50, 70));
        lateWorkersPanel.add(jLabel7, java.awt.BorderLayout.CENTER);

        jPanel5.setPreferredSize(new java.awt.Dimension(100, 70));

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 10)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(177, 152, 220));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Late Workers");
        jLabel8.setToolTipText("");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel8.setName(""); // NOI18N
        jLabel8.setOpaque(true);
        jLabel8.setPreferredSize(new java.awt.Dimension(100, 35));

        totalWorkersLabel3.setBackground(new java.awt.Color(255, 255, 255));
        totalWorkersLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        totalWorkersLabel3.setForeground(new java.awt.Color(33, 63, 87));
        totalWorkersLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        totalWorkersLabel3.setText("0");
        totalWorkersLabel3.setToolTipText("");
        totalWorkersLabel3.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        totalWorkersLabel3.setName(""); // NOI18N
        totalWorkersLabel3.setOpaque(true);
        totalWorkersLabel3.setPreferredSize(new java.awt.Dimension(100, 35));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addComponent(totalWorkersLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(totalWorkersLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        lateWorkersPanel.add(jPanel5, java.awt.BorderLayout.EAST);

        jPanel1.add(lateWorkersPanel);

        EarlyWorkersPanel.setLayout(new java.awt.BorderLayout());

        jLabel3.setBackground(new java.awt.Color(109, 199, 190));
        jLabel3.setFont(new java.awt.Font("Cambria Math", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(33, 63, 87));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/early_workers.png"))); // NOI18N
        jLabel3.setName(""); // NOI18N
        jLabel3.setOpaque(true);
        jLabel3.setPreferredSize(new java.awt.Dimension(50, 70));
        EarlyWorkersPanel.add(jLabel3, java.awt.BorderLayout.CENTER);

        jPanel3.setPreferredSize(new java.awt.Dimension(100, 70));

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 10)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(109, 199, 190));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Early Workers");
        jLabel4.setToolTipText("");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel4.setName(""); // NOI18N
        jLabel4.setOpaque(true);
        jLabel4.setPreferredSize(new java.awt.Dimension(100, 35));

        totalWorkersLabel1.setBackground(new java.awt.Color(255, 255, 255));
        totalWorkersLabel1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        totalWorkersLabel1.setForeground(new java.awt.Color(33, 63, 87));
        totalWorkersLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        totalWorkersLabel1.setText("0");
        totalWorkersLabel1.setToolTipText("");
        totalWorkersLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        totalWorkersLabel1.setName(""); // NOI18N
        totalWorkersLabel1.setOpaque(true);
        totalWorkersLabel1.setPreferredSize(new java.awt.Dimension(100, 35));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(totalWorkersLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(totalWorkersLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        EarlyWorkersPanel.add(jPanel3, java.awt.BorderLayout.EAST);

        jPanel1.add(EarlyWorkersPanel);

        graphPanel.setBackground(new java.awt.Color(255, 255, 255));
        graphPanel.setPreferredSize(new java.awt.Dimension(400, 350));
        graphPanel.setLayout(new java.awt.BorderLayout());

        jPanel7.setPreferredSize(new java.awt.Dimension(350, 350));
        jPanel7.setLayout(new java.awt.BorderLayout());

        tableheaderPanel.setPreferredSize(new java.awt.Dimension(350, 30));

        jLabel10.setFont(new java.awt.Font("Cambria", 0, 12)); // NOI18N
        jLabel10.setText("Filter:");
        jLabel10.setToolTipText("");
        jLabel10.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel10.setPreferredSize(new java.awt.Dimension(30, 30));

        datePickerPanel.setPreferredSize(new java.awt.Dimension(120, 30));
        datePickerPanel.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout tableheaderPanelLayout = new javax.swing.GroupLayout(tableheaderPanel);
        tableheaderPanel.setLayout(tableheaderPanelLayout);
        tableheaderPanelLayout.setHorizontalGroup(
            tableheaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableheaderPanelLayout.createSequentialGroup()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(datePickerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 179, Short.MAX_VALUE))
        );
        tableheaderPanelLayout.setVerticalGroup(
            tableheaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tableheaderPanelLayout.createSequentialGroup()
                .addGroup(tableheaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(datePickerPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel7.add(tableheaderPanel, java.awt.BorderLayout.PAGE_START);

        tablePanel.setToolTipText("");
        tablePanel.setPreferredSize(new java.awt.Dimension(350, 300));
        tablePanel.setLayout(new java.awt.BorderLayout());
        jPanel7.add(tablePanel, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout containerLayout = new javax.swing.GroupLayout(container);
        container.setLayout(containerLayout);
        containerLayout.setHorizontalGroup(
            containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 789, Short.MAX_VALUE)
            .addGroup(containerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(graphPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        containerLayout.setVerticalGroup(
            containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(containerLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addGroup(containerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(graphPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.getAccessibleContext().setAccessibleName("");

        add(container, java.awt.BorderLayout.CENTER);

        jPanel6.setBackground(new java.awt.Color(232, 232, 232));
        jPanel6.setToolTipText("");
        jPanel6.setPreferredSize(new java.awt.Dimension(800, 50));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        add(jPanel6, java.awt.BorderLayout.PAGE_START);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel DailyAttendancePanel;
    private javax.swing.JPanel EarlyWorkersPanel;
    private javax.swing.JPanel container;
    private javax.swing.JLabel dailyAttendancelabel;
    private javax.swing.JLabel dateLabel;
    private javax.swing.JPanel datePickerPanel;
    private javax.swing.JPanel graphPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel lateWorkersPanel;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JPanel tableheaderPanel;
    private javax.swing.JLabel totalWorkersLabel;
    private javax.swing.JLabel totalWorkersLabel1;
    private javax.swing.JLabel totalWorkersLabel3;
    private javax.swing.JPanel totalWorkerspanel;
    // End of variables declaration//GEN-END:variables
}
