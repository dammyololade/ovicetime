/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime;

import com.jfoenix.controls.JFXDatePicker;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.jdatepicker.JDatePanel;
import org.jdatepicker.impl.*;
import org.jdatepicker.util.*;
import org.jdatepicker.*;
import oviceTime.database.DbHelper;
import oviceTime.ui.TmScrollBar;
import oviceTime.utilities.Utilities;
import oviceTime.utilities.TableRenderer;

/**
 *
 * @author user
 */
public class RecordsPanel extends javax.swing.JPanel {
    
    public DbHelper dbHelper;
    private String selectedName;
    private final String[] columnNames = {"S/N", "Fullname", " Date ", "Time-In", " Time-Out "};
    private String [][] data;
    private JTable table;
    private JDatePanelImpl datePanel2;
    private JDatePickerImpl datePicker2;
    private final String today;

    /**
     * Creates new form RecordsPanel
     */
    public RecordsPanel() {
        initComponents();
        dbHelper = new DbHelper(true);
        today = Utilities.todayDate();
        createDailyRecordTable();
        
        setupCalender();
    }
    
    private void setupCalender(){
        
        UtilDateModel model = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        
        JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
        JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
        JFormattedTextField textField = datePicker.getJFormattedTextField();
        textField.setFont(new Font("Cambria", Font.PLAIN, 12));
        textField.setBackground(Color.white);
        textField.setBorder(new MatteBorder(0, 0, 2, 0, Color.BLACK));
        textField.setText("Select A date");
        
        datePicker.setBorder(new EmptyBorder(20, 0, 0, 0));
        datePicker.setBackground(new Color(232,232,232));
        headerPanel.add(datePicker);
        
        
    }
    
    /**
     * create the record for each day
     */
    private void createDailyRecordTable() {
         tablePanel.removeAll();
         
         
          try{
                data = dbHelper.getDailyAttendanceRecords();
                
                if(data.length > 0){
                    
                    table = new JTable(data, columnNames);
                    table.setSelectionMode(1);
                    table.setRowHeight(30);      
                    table.setDefaultRenderer(Object.class, new TableRenderer());
                    table.setSelectionBackground(new Color(204,204,204));
                    table.setSelectionForeground(Color.black);
                    
                    table.setBackground(Color.white);
                    table.setBorder(new EmptyBorder(8, 8, 8, 8));
                    table.setGridColor(Color.white);

                    JScrollPane scrollPane = new JScrollPane(table);
                    scrollPane.getVerticalScrollBar().setUI(new TmScrollBar());
                    scrollPane.setBackground(new Color(240,240,240));
                    scrollPane.setBorder(new EmptyBorder(8, 8, 8, 8));
                    tablePanel.add(scrollPane, BorderLayout.CENTER);

                    table.setBorder(null);
                    table.getSelectionModel().addListSelectionListener(new RecordsPanel.TableSelectionListener());
                }else{
                    JLabel label = new JLabel("No Record Found");
                    label.setOpaque(true);
                    label.setForeground(new Color(47,58,74));
                    label.setFont(new Font("Cambria", Font.BOLD, 18));
                    label.setBorder(new EmptyBorder(200, 200, 200, 200));
                    tablePanel.add(label);
                }
                
                
         }catch(NullPointerException dd){
                dd.printStackTrace();
         }
         
    }
    
    /**
     * create a table with a record that has been filtered by date
     * @param date 
     */ 
    private void createFilteredRecordTable(String date) {
         tablePanel.removeAll();
         tablePanel.revalidate();         
          try{
                
                data = dbHelper.getAttendanceRecordsFromDate(date, today);
                if(data.length > 0){
                    
                    table = new JTable(data, columnNames);
                    table.setSelectionMode(1);
                    table.setRowHeight(30);      
                    table.setDefaultRenderer(Object.class, new TableRenderer());
                    table.setSelectionBackground(new Color(204,204,204));
                    table.setSelectionForeground(Color.black);
                    
                    table.setBackground(Color.white);
                    table.setBorder(new EmptyBorder(8, 8, 8, 8));
                    table.setGridColor(Color.white);

                    JScrollPane scrollPane = new JScrollPane(table);
                    scrollPane.getVerticalScrollBar().setUI(new TmScrollBar());
                    scrollPane.setBackground(new Color(240,240,240));
                    scrollPane.setBorder(new EmptyBorder(8, 8, 8, 8));
                    tablePanel.add(scrollPane, BorderLayout.CENTER);

                    table.setBorder(null);
                    table.getSelectionModel().addListSelectionListener(new RecordsPanel.TableSelectionListener());
                }else{
                    JLabel label = new JLabel("No Record Found");
                    label.setOpaque(true);
                    label.setForeground(new Color(47,58,74));
                    label.setFont(new Font("Cambria", Font.PLAIN, 18));
                    label.setBorder(new EmptyBorder(100, 300, 0, 0));
                    tablePanel.add(label, BorderLayout.NORTH);
                }
                
                
         }catch(NullPointerException dd){
                dd.printStackTrace();
         }
         
    }
    /**
     * 
     * @param courseId 
     */
    public void referesh(){
        createDailyRecordTable();
        
    }
    
    /**
     * this segments listens to the click event on the table
     * selects the date equivalent of the attendance of the student selected
     * used some array tricks
     */
    
    private class TableSelectionListener implements ListSelectionListener{
        
        @Override
        public void valueChanged(ListSelectionEvent e) {
           
            int row = table.getSelectedRow();//gets the index of the selected row
            String ft[] = data[row];
            //System.out.println("the selected row is  " + ft[0].toString());
            
           
        }  
    }
    
    public class DateLabelFormatter extends AbstractFormatter{
            
        private String datePattern = "yyyy-MM-dd";
        private SimpleDateFormat dateFormattter = new SimpleDateFormat(datePattern);
        
        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormattter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) throws ParseException {
            if(value != null){
                Calendar cal = (Calendar) value;
                String selDate = dateFormattter.format(cal.getTime());
                createFilteredRecordTable(selDate);
                return selDate;
            }
            
            return "";
        }
    
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tablePanel = new javax.swing.JPanel();
        headerPanel = new javax.swing.JPanel();

        setPreferredSize(new java.awt.Dimension(800, 550));
        setLayout(new java.awt.BorderLayout());

        tablePanel.setPreferredSize(new java.awt.Dimension(800, 500));
        tablePanel.setLayout(new java.awt.BorderLayout());
        add(tablePanel, java.awt.BorderLayout.CENTER);

        headerPanel.setBackground(new java.awt.Color(232, 232, 232));
        headerPanel.setToolTipText("");
        headerPanel.setPreferredSize(new java.awt.Dimension(800, 50));
        add(headerPanel, java.awt.BorderLayout.PAGE_START);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel headerPanel;
    private javax.swing.JPanel tablePanel;
    // End of variables declaration//GEN-END:variables

    
}
