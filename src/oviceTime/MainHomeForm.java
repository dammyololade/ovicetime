/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafxbeta.StaffRegistrationController;
import javafxbeta.StaffRegistrationForm;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import oviceTime.database.DbHelper;
import oviceTime.dialogs.AttendanceDialog;
import oviceTime.dialogs.Registration;

import oviceTime.model.FingerModel;
import oviceTime.model.StaffModel;
import oviceTime.ui.TmPopupContainer;
import oviceTime.ui.TmScrollBar;
import oviceTime.listeners.OnAttendanceMarkedListener;
import oviceTime.listeners.OnUpdateListener;
import oviceTime.utilities.DPFPProvider;
import oviceTime.utilities.DragUndecoratedWindow;
import oviceTime.utilities.StaffSearchListRenderer;

/**
 *
 * @author user
 */
public class MainHomeForm extends javax.swing.JFrame implements OnUpdateListener, OnAttendanceMarkedListener{

    private static Stage stage;
    
    private DbHelper dbHelper;
    public static DPFPProvider dProvider;

    public static boolean isFPConnected = false;
    public static boolean isFPSensorReady = false;
    public static boolean isFPDataReady = false;
    public static boolean isFPImageQualityGood = false;
    public static JFrame windowFrame;
    
    final static String STAFF_LIST_PANEL = "staffListPanel";
    final static String STAFF_DETAILS_PANEL = "staffDetailsPanel";
     final static String SUMMARY_PANEL = "summaryPanel";
     final static String RECORDS_PANEL = "recordsPanel";
    public static StaffListPanel staffListPanel;
    public static SummaryPanel summaryPanel;
    public static RecordsPanel recordsPanel;
    public static StaffDetailsPanel staffDetailsPanel;
     private List<FingerModel> fingerList = null;
     JLabel homeLabel;
     public static String fingerstring;
     
     Point mouseDownCompCoords = null;
    private List<StaffModel> staffs;
    private DefaultListModel<StaffModel> staffmodel;
    private JList<StaffModel> staffJList;
    private TmPopupContainer popMenu;

    /**
     * Creates new form MainHomeForm
     */
    public MainHomeForm() {
        
        this.setUndecorated(true);
        dbHelper = new DbHelper(true);
        dProvider = new DPFPProvider(this);
        windowFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
        
        initComponents();
        
        init();
        setUpCardLayout();
       // se=tUpCardLayout();
        homeLabel = dashboardlabel;
        List<Image> icons = new ArrayList<Image>();            
            icons.add(new ImageIcon(getClass().getResource("/oviceTime/icon/" + "fingerprint-dark-2" + ".png")).getImage());
            this.setIconImages(icons);
//            FrameDragListener frameDragListener = new FrameDragListener(this);
//                this.addMouseListener(frameDragListener);
//                this.addMouseMotionListener(frameDragListener);
         setLabelBackground(dashboardlabel);
    }
    
     private void setUpCardLayout(){
        
        MainHomeForm.staffListPanel = new StaffListPanel();
        staffListPanel.windowFrame = this.windowFrame;       
        MainHomeForm.summaryPanel = new SummaryPanel();
        MainHomeForm.recordsPanel = new RecordsPanel();
        MainHomeForm.staffDetailsPanel = new StaffDetailsPanel();
        
       // cardsContainer.add(new MyCoursesForm(), COURSE_PANEL);
        cardsContainer.add(MainHomeForm.summaryPanel, SUMMARY_PANEL);
        cardsContainer.add(MainHomeForm.recordsPanel, RECORDS_PANEL);
        cardsContainer.add(MainHomeForm.staffListPanel, STAFF_LIST_PANEL);
        cardsContainer.add(MainHomeForm.staffDetailsPanel, STAFF_DETAILS_PANEL);
       
    }
     
   
    
    public void switchCardsInHome(String cardName){
        CardLayout cardLayout = (CardLayout) cardsContainer.getLayout();
        cardLayout.show(cardsContainer, cardName);
    }
    
     private void init(){
        //Call initialize capturer for finger print
        initCapturer();
        
        this.setTitle("OvTime");
        this.setLocationRelativeTo(null);
        this.getContentPane().setBackground(new Color(33,63,87));
        
//        MyCoursesForm myCoursesForm = new MyCoursesForm();
//        homeContainerScrollPane.setViewportView(myCoursesForm);

    }
    
    private void initCapturer(){
        this.addComponentListener(new ComponentAdapter() {
            @Override public void componentShown(ComponentEvent e) {
                System.out.println("init capture in add component is shown i.e component are shown in dialog");

                dProvider.init();
                //Keep checking if the listener has been connected
                dProvider.start();
                
            }
            @Override public void componentHidden(ComponentEvent e) {
                System.out.println("inicapture in add component is hidden i.e component are hiiden in attendance dialog");

                dProvider.stop();
            }
        });
    }
    
    private Border getPanelBorder(){
        
        Border padding = BorderFactory.createEmptyBorder(8, 8, 8, 8);
        Border leftBorder = BorderFactory.createMatteBorder(0, 3, 0, 0, new Color(0, 191, 233));
        return BorderFactory.createCompoundBorder(leftBorder, padding);
    }
    
    private Border getPanelBorder(boolean empty){
        return new EmptyBorder(8, 8, 8, 8);
    }
    
    public void loadFingers(){
        
        fingerList = dbHelper.getAllFingers();
    }
    
     private void finalise(){
        
        this.setVisible(false);
       
        try {
            this.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(MainHomeForm.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
     
    private void setLabelBackground(JLabel lbl){
        homeLabel = lbl;
        lbl.setFont(new Font(getFont().getName(), Font.PLAIN, 14));
        lbl.setBorder(getPanelBorder());
        lbl.setBackground(Color.black);
    }
    private void resetLabelBackground(JLabel lbl){
        
        lbl.setFont(new Font(getFont().getName(), Font.PLAIN, 14));
        lbl.setBorder(getPanelBorder(true));
        lbl.setBackground(new Color(42, 51, 68));
    }
    
    
    public void searchmethod(){
        String input = searchField.getText();
        if(!input.isEmpty() && !input.equalsIgnoreCase("_")){
            
                Point location = searchField.getLocationOnScreen();
            //int x = (int) location.getX(); //Just the button pos b4
            int x = this.getX() + this.getWidth();
            int y = (int) (location.getY() + searchField.getHeight());

            //System.out.println("Xpos:" + x + " Ypos:" + y);

            SearchDropDownMenu menu = new SearchDropDownMenu(this);
            JScrollPane scrollPane = new JScrollPane(staffJList = createStudentList());
            
            if(staffs.size() != 0){
                
                
                scrollPane.setPreferredSize(new java.awt.Dimension(200, 170));
                scrollPane.setBorder(null);
                scrollPane.getVerticalScrollBar().setUI(new TmScrollBar());
                menu.add(scrollPane, BorderLayout.CENTER);
            }
            else{
                 JLabel mess = new JLabel();
                 mess.setText("No result found............");
                 mess.setFont(new Font(getFont().getName(), Font.CENTER_BASELINE, 12));
                 mess.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
                 mess.setVerticalAlignment(javax.swing.SwingConstants.TOP);
                // menu.setBackground(new Color(60,63,95));
                
                 mess.setForeground(Color.white);
                  menu.add(mess, BorderLayout.CENTER);
                  String sd = mess.getText();
            
            }          


            //Get the width of this panel;
            //x = (x - ((int)menu.getPreferredSize().getWidth() / 2));
            x = (x - ((int)menu.getPreferredSize().getWidth()));
            x = x - 400;
            y = y + 10;


            //System.out.println( "width:" + menu.getPreferredSize().getWidth() + "new Xpos:" + x);

            popMenu = new TmPopupContainer(this, menu, x, y);
            popMenu.show();        
        }
        
           
    }   
    
     /**
     * this creates the search list fro the database and returns it as an arrayList
     * @return JList
     */
    private JList<StaffModel> createStudentList(){
        
        String input = searchField.getText().toLowerCase();//converts the input xter into lower cse to search
        
        ////////////////// DB Search Student ///////////////////////////
        staffs = dbHelper.searchStaff(input);
        ///////////////////////////////////////////////////////////////
        
        staffmodel = new DefaultListModel<>();
        
        if(staffs != null){
            for(int i = 0; i < staffs.size(); i++){
                staffmodel.addElement(staffs.get(i));
            }
        }else{ 
            
            
        }
        
        JList<StaffModel> jlist = new JList<>(staffmodel); //Set the model for list here
        
            //Set the renderer here too.
            jlist.setCellRenderer(new StaffSearchListRenderer());

            jlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            //list.setPreferredSize(new java.awt.Dimension(280, 540));
//            jlist.setSelectionBackground(Color.BLACK);
//            jlist.setSelectionForeground(Color.white);
//            jlist.setBackground(new Color(47,58,74));
             jlist.setSelectionBackground(Color.BLACK);
            jlist.setSelectionForeground(Color.white);
            jlist.setBackground(Color.white);
            jlist.setBorder(null);
            if (staffmodel.size() > 0){
                 jlist.setSelectedIndex(0);            
            } 
            
            jlist.addMouseListener(new SearchListSelectionListener());
      return jlist;
    
    
    }

    @Override
    public void onStaffDeleted() {
       
    }
    
      /**
     * this is the search list listener
     * listens to the mouse events
     * and perform specified instructions
     */
    
    private class SearchListSelectionListener extends MouseAdapter{

        
        public SearchListSelectionListener() {
            
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            JList list = (JList) e.getSource();
            try{
                StaffModel data =  staffJList.getSelectedValue();
                String fullname = data.getStaffFullname();
                resetLabelBackground(homeLabel);
                setLabelBackground(details);
                switchCardsInHome(STAFF_LIST_PANEL);
                staffListPanel.loadStaffDetails(fullname);
                popMenu.hide();
                searchField.setText("");
                   
            }catch(NullPointerException sd){
            
            }
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sidePanel = new javax.swing.JPanel();
        details = new javax.swing.JLabel();
        dashboardlabel = new javax.swing.JLabel();
        admin2 = new javax.swing.JLabel();
        records = new javax.swing.JLabel();
        mainPanel = new javax.swing.JPanel();
        topPanel = new javax.swing.JPanel();
        powerBtn = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        searchField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cardsContainer = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocationByPlatform(true);
        setUndecorated(true);
        setResizable(false);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                formMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });

        sidePanel.setBackground(new java.awt.Color(47, 58, 74));
        sidePanel.setToolTipText("");
        sidePanel.setPreferredSize(new java.awt.Dimension(200, 600));

        details.setBackground(new java.awt.Color(47, 58, 74));
        details.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        details.setForeground(java.awt.Color.white);
        details.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/details.png"))); // NOI18N
        details.setText("Staff Details");
        details.setToolTipText("");
        details.setAlignmentX(0.5F);
        details.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 10, 1, 1));
        details.setIconTextGap(20);
        details.setName(""); // NOI18N
        details.setOpaque(true);
        details.setPreferredSize(new java.awt.Dimension(200, 40));
        details.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                detailsMouseClicked(evt);
            }
        });

        dashboardlabel.setBackground(new java.awt.Color(47, 58, 74));
        dashboardlabel.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        dashboardlabel.setForeground(java.awt.Color.white);
        dashboardlabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/Home_24px.png"))); // NOI18N
        dashboardlabel.setText("Dashboard");
        dashboardlabel.setToolTipText("");
        dashboardlabel.setAlignmentX(0.5F);
        dashboardlabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 10, 1, 1));
        dashboardlabel.setIconTextGap(20);
        dashboardlabel.setName(""); // NOI18N
        dashboardlabel.setOpaque(true);
        dashboardlabel.setPreferredSize(new java.awt.Dimension(200, 40));
        dashboardlabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dashboardlabelMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                dashboardlabelMouseEntered(evt);
            }
        });

        admin2.setBackground(new java.awt.Color(0, 191, 233));
        admin2.setFont(new java.awt.Font("Cambria", 1, 18)); // NOI18N
        admin2.setForeground(java.awt.Color.white);
        admin2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/logo.png"))); // NOI18N
        admin2.setText("OvTime");
        admin2.setToolTipText("");
        admin2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 20, 1, 1));
        admin2.setIconTextGap(10);
        admin2.setName(""); // NOI18N
        admin2.setOpaque(true);
        admin2.setPreferredSize(new java.awt.Dimension(92, 50));

        records.setBackground(new java.awt.Color(47, 58, 74));
        records.setFont(new java.awt.Font("Cambria", 0, 14)); // NOI18N
        records.setForeground(java.awt.Color.white);
        records.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/records (2).png"))); // NOI18N
        records.setText("Records");
        records.setToolTipText("");
        records.setAlignmentX(0.5F);
        records.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 13, 1, 1));
        records.setIconTextGap(20);
        records.setName(""); // NOI18N
        records.setOpaque(true);
        records.setPreferredSize(new java.awt.Dimension(200, 40));
        records.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                recordsMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout sidePanelLayout = new javax.swing.GroupLayout(sidePanel);
        sidePanel.setLayout(sidePanelLayout);
        sidePanelLayout.setHorizontalGroup(
            sidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sidePanelLayout.createSequentialGroup()
                .addGroup(sidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(records, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(details, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(sidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(admin2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dashboardlabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        sidePanelLayout.setVerticalGroup(
            sidePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sidePanelLayout.createSequentialGroup()
                .addComponent(admin2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(77, 77, 77)
                .addComponent(dashboardlabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(details, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(records, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(336, Short.MAX_VALUE))
        );

        getContentPane().add(sidePanel, java.awt.BorderLayout.WEST);

        mainPanel.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 1, 1, new java.awt.Color(47, 58, 74)));
        mainPanel.setPreferredSize(new java.awt.Dimension(800, 600));
        mainPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                mainPanelMouseDragged(evt);
            }
        });
        mainPanel.setLayout(new java.awt.BorderLayout());

        topPanel.setBackground(new java.awt.Color(255, 255, 255));
        topPanel.setToolTipText("");
        topPanel.setPreferredSize(new java.awt.Dimension(800, 50));
        topPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                topPanelMouseDragged(evt);
            }
        });
        topPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                topPanelMousePressed(evt);
            }
        });

        powerBtn.setBackground(new java.awt.Color(33, 63, 87));
        powerBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/Menu_18px.png"))); // NOI18N
        powerBtn.setToolTipText("Menu");
        powerBtn.setBorder(null);
        powerBtn.setBorderPainted(false);
        powerBtn.setContentAreaFilled(false);
        powerBtn.setFocusPainted(false);
        powerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                powerBtnActionPerformed(evt);
            }
        });

        jSeparator1.setPreferredSize(new java.awt.Dimension(200, 5));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/magnify-3.png"))); // NOI18N
        jLabel1.setPreferredSize(new java.awt.Dimension(24, 15));

        searchField.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        searchField.setBorder(null);
        searchField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchFieldActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/add_user.png"))); // NOI18N
        jLabel2.setToolTipText("Create new Profile");
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/lock 2_18px.png"))); // NOI18N
        jLabel3.setToolTipText("Lock");
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/attendance_18px.png"))); // NOI18N
        jLabel4.setToolTipText("Take Attendance");
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });

        jLabel6.setBackground(new java.awt.Color(232, 232, 232));
        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/send_report.png"))); // NOI18N
        jLabel6.setToolTipText("send report");
        jLabel6.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout topPanelLayout = new javax.swing.GroupLayout(topPanel);
        topPanel.setLayout(topPanelLayout);
        topPanelLayout.setHorizontalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(topPanelLayout.createSequentialGroup()
                .addGap(65, 65, 65)
                .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(topPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(searchField, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 276, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(33, 33, 33)
                .addComponent(jLabel4)
                .addGap(27, 27, 27)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jLabel3)
                .addGap(29, 29, 29)
                .addComponent(powerBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );
        topPanelLayout.setVerticalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                        .addComponent(powerBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(topPanelLayout.createSequentialGroup()
                        .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(searchField, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, 0)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mainPanel.add(topPanel, java.awt.BorderLayout.PAGE_START);

        cardsContainer.setPreferredSize(new java.awt.Dimension(800, 550));
        cardsContainer.setLayout(new java.awt.CardLayout());
        mainPanel.add(cardsContainer, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(mainPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void powerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_powerBtnActionPerformed

        Point location = powerBtn.getLocationOnScreen();
        //int x = (int) location.getX(); //Just the button pos b4
        int x = this.getX() + this.getWidth();
        int y = (int) (location.getY() + powerBtn.getHeight());

        //System.out.println("Xpos:" + x + " Ypos:" + y);

        DropDownMenuPanel menu = new DropDownMenuPanel(this);

        //Get the width of this panel;
        //x = (x - ((int)menu.getPreferredSize().getWidth() / 2));
        x = (x - ((int)menu.getPreferredSize().getWidth()));
        x = x - 3;
        y = y + 10;

        //System.out.println( "width:" + menu.getPreferredSize().getWidth() + "new Xpos:" + x);

        TmPopupContainer popMenu = new TmPopupContainer(this, menu, x, y);
        popMenu.show();

    }//GEN-LAST:event_powerBtnActionPerformed

    private void detailsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_detailsMouseClicked
        // TODO add your handling code here:
        resetLabelBackground(homeLabel);
        setLabelBackground(details);
        switchCardsInHome(STAFF_LIST_PANEL);
    }//GEN-LAST:event_detailsMouseClicked

    private void dashboardlabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dashboardlabelMouseEntered
        // TODO add your handling code here:
      
    }//GEN-LAST:event_dashboardlabelMouseEntered

    private void recordsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_recordsMouseClicked
        // TODO add your handling code here:
        resetLabelBackground(homeLabel);
        setLabelBackground(records);
        switchCardsInHome(RECORDS_PANEL);
    }//GEN-LAST:event_recordsMouseClicked

    private void dashboardlabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dashboardlabelMouseClicked
        // TODO add your handling code here:
        resetLabelBackground(homeLabel);
        setLabelBackground(dashboardlabel);
        
        switchCardsInHome(SUMMARY_PANEL);
    }//GEN-LAST:event_dashboardlabelMouseClicked

    private void formMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseDragged
        // TODO add your handling code here:
        DragUndecoratedWindow window = new DragUndecoratedWindow(this);
        window.onPress(evt);
    }//GEN-LAST:event_formMouseDragged

    private void mainPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mainPanelMouseDragged
        // TODO add your handling code here:
         
    }//GEN-LAST:event_mainPanelMouseDragged

    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
        // TODO add your handling code here:
        DragUndecoratedWindow window = new DragUndecoratedWindow(this);
        window.moveWindow(evt);
    }//GEN-LAST:event_formMouseMoved

    private void searchFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchFieldActionPerformed
        // TODO add your handling code here:
        searchmethod();
    }//GEN-LAST:event_searchFieldActionPerformed

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        dProvider.stop();        

        Registration reg = new Registration(windowFrame, true);
        reg.setOnUpdateStaffListener(this);
        reg.setVisible(true);  
        
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        // TODO add your handling code here:
         dProvider.stop();        
        loadFingers();
        AttendanceDialog dialog = new AttendanceDialog(windowFrame, true);
        dialog.setFingerList(fingerList);
        dialog.setOnAttendanceMarkedlistener(this);
        dialog.setVisible(true);
    }//GEN-LAST:event_jLabel4MouseClicked

    private void topPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topPanelMouseDragged
        // TODO add your handling code here:
         
         Point currCoords = evt.getLocationOnScreen();
         this.setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
    }//GEN-LAST:event_topPanelMouseDragged

    private void topPanelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topPanelMousePressed
        // TODO add your handling code here:
        mouseDownCompCoords = evt.getPoint();
    }//GEN-LAST:event_topPanelMousePressed

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        // TODO add your handling code here:
        dProvider.stop();
        LoginForm form = new LoginForm();
        form.setVisible(true);
        this.finalise();
    }//GEN-LAST:event_jLabel3MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainHomeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainHomeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainHomeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainHomeForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainHomeForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel admin2;
    private javax.swing.JPanel cardsContainer;
    private javax.swing.JLabel dashboardlabel;
    private javax.swing.JLabel details;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton powerBtn;
    private javax.swing.JLabel records;
    private javax.swing.JTextField searchField;
    private javax.swing.JPanel sidePanel;
    private javax.swing.JPanel topPanel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onUpdateStaff(String fullname) {
        
        resetLabelBackground(homeLabel);
        setLabelBackground(details);
        switchCardsInHome(STAFF_LIST_PANEL);
        staffListPanel.loadStaffDetails(fullname);
    }

    @Override
    public void OnAttendanceMarked() {
        summaryPanel.refresh();
        switchCardsInHome(SUMMARY_PANEL);
    }
    
     public static class FrameDragListener extends MouseAdapter {

        private final JFrame frame;
        private Point mouseDownCompCoords = null;

        public FrameDragListener(JFrame frame) {
            this.frame = frame;
        }

        public void mouseReleased(MouseEvent e) {
            mouseDownCompCoords = null;
        }

        public void mousePressed(MouseEvent e) {
            mouseDownCompCoords = e.getPoint();
        }

        public void mouseDragged(MouseEvent e) {
            Point currCoords = e.getLocationOnScreen();
            frame.setLocation(currCoords.x - mouseDownCompCoords.x, currCoords.y - mouseDownCompCoords.y);
        }
    }
}
