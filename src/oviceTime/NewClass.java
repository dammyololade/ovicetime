/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime;

/**
 *
 * @author user
 */
import javafx.application.Platform;
import javafx.beans.property.SimpleListProperty;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.layout.GridPane;

import javax.swing.*;
import java.awt.*;
import javafx.scene.Group;

public class NewClass {

  public static void main ( String[] args){
      
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        ChartFrame mainFrame = new ChartFrame();
        mainFrame.setVisible(true);
        }
      });
    }
  }

class ChartFrame extends JFrame {

  JFXPanel fxPanel;
  public ChartFrame(){
    initSwingComponents();

    initFxComponents();
  }

  private void initSwingComponents(){
    JPanel mainPanel = new JPanel(new BorderLayout());
    fxPanel = new JFXPanel();
    mainPanel.add(fxPanel, BorderLayout.CENTER);

    JLabel titleLabel = new JLabel("Charts in Swing applications");
    mainPanel.add(titleLabel, BorderLayout.NORTH);

    this.add(mainPanel);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setSize(800,400);
  }

  private void initFxComponents(){

        Platform.runLater(new Runnable() {
          @Override
          public void run() {
             //Defining the x axis             
          NumberAxis xAxis = new NumberAxis(1960, 2020, 10); 
          xAxis.setLabel("Years"); 

          //Defining the y axis   
          NumberAxis yAxis = new NumberAxis   (0, 350, 50); 
          yAxis.setLabel("No.of schools"); 

          //Creating the line chart 
          LineChart linechart = new LineChart(xAxis, yAxis);  

          //Prepare XYChart.Series objects by setting data 
          XYChart.Series series = new XYChart.Series(); 
          series.setName("No of schools in an year"); 

          series.getData().add(new XYChart.Data(1970, 15)); 
          series.getData().add(new XYChart.Data(1980, 30)); 
          series.getData().add(new XYChart.Data(1990, 60)); 
          series.getData().add(new XYChart.Data(2000, 120)); 
          series.getData().add(new XYChart.Data(2013, 240)); 
          series.getData().add(new XYChart.Data(2014, 300)); 

          //Setting the data to Line chart    
          linechart.getData().add(series);        

          //Creating a Group object  
          Group root = new Group(linechart); 

          //Creating a scene object 
          Scene scene = new Scene(root, 600, 400);  


              fxPanel.setScene(scene);
        }
      });

     }

  }

