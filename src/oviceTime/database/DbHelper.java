/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import oviceTime.model.AdminModel;
import oviceTime.model.AttendanceModel;
import oviceTime.model.FingerModel;
import oviceTime.model.StaffModel;

/**
 *
 * @author user
 */
public class DbHelper {
    
    private Connection connection = null; // Manages Connection
    private Statement statement = null;
    private PreparedStatement newstate = null;
    private ResultSet resultSet = null;
    private ResultSet result = null;
    private PreparedStatement prepStatement = null;
    
    //For setting up derby db connection
    private final String DB_NAME = "ibile";
    private final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    //private final String DB_NET_DRIVER = "org.apache.derby.jdbc.ClientDriver";
    
    private final String DERBY_CONN_URL = "jdbc:derby:"+ DB_NAME + ";create=true";
    //private final String DERBY_NET_CONN_URL = "jdbc:derby://localhost:1527/"+ DB_NAME+";create=true";
    
    private static final String DB_USERNAME = "ibile_user";
    private static final String DB_PASSWORD = "ibile_password";
    

    public DbHelper() {
        this.connection = null;
    }
    
     /**
     * 
     * @param connect
     */
    public DbHelper(boolean connect){
        this.connect();
    }
    
    /**
     * Making connection to the database, must be called before every other database interaction
     * and a close type must be called to release all resource when done connecting to database.
     * @return boolean true/false for connection
     */
    public boolean connect(){
        boolean connected;
    
            try {
           
                //Class.forName(DB_DRIVER); //note: Application do not need this anymore
            
                connection = DriverManager.getConnection(DERBY_CONN_URL);//, DB_USERNAME, DB_PASSWORD);
                
            } catch (SQLException ex) {
                System.out.println("Connection Failed! Check output console");
                
                ex.printStackTrace();
                //shutDownDatabase();
            }

            if(connection != null){
                System.out.println("Connected to database " + DB_NAME);
                //Init database tables here
                initDb();
                connected = true;
            }else{
                System.out.println("database connection to " + DB_NAME + " failed");
                connected = false;
            }
        

        return connected;
        
    }
    
     /**
    * For closing connection to the database
    */
    public void close(){
        try {
            connection.close();
            System.out.println("closing database connection");
        } catch (SQLException ex) {
            Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean shutDownDatabase(){
        
        boolean shutDownComplete = false;
        if (DB_DRIVER.equals("org.apache.derby.jdbc.EmbeddedDriver")) {
            try {
               DriverManager.getConnection("jdbc:derby:;shutdown=true");//Perform the Shutdown
            } catch (SQLException se)  {	
               if ( se.getSQLState().equals("XJ015") ) {		
                  shutDownComplete = true;
               }
            }
            if (!shutDownComplete) {
               System.out.println("Database did not shut down normally");
            }  else  {
               System.out.println("Database shut down normally");	
            }  
        }
        return shutDownComplete;
        
    }    
    
     public static boolean checkIfTableExist(Connection conn, String table) throws SQLException{
        
        try {
           Statement s = conn.createStatement();
           s.execute("SELECT * FROM "+ table);
           
        }  catch (SQLException sqlex) {
            
           String theError = (sqlex).getSQLState();
           
           /** If table exists will get -  WARNING 02000: No row was found **/
           if (theError.equals("42X05"))   // Table does not exist
           {
               System.out.println(table + " : Table does not yet exist.");
               return false;
           }else { 
                System.out.println("checkIfTableExist(): Unhandled SQLException : i no know d error");
                throw sqlex;
           }
           
        }
        System.out.println(table + " : Table Exists and Doing fine. ");
        return true;
        
    } //End checkIfTableExist
     
    public void initDb(){
      
         String createAdminSQL = "CREATE TABLE admin ( "
                + "admin_id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
                + "staff_id INTEGER NOT NULL,"
                + "fingerprint LONG VARCHAR,"  
                + "password VARCHAR(150),"   
                + "last_login_date DATE,"
                + "last_login_time TIME )";
         
         String createStaffSQL = "CREATE TABLE staffs ( "
                + "staff_id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
                + "fullname VARCHAR(150) NOT NULL,"
                + "fingerprint LONG VARCHAR,"
                + "address VARCHAR(150),"                
                + "number VARCHAR(150),"
                + "email VARCHAR(150),"
                + "blood_group VARCHAR(150),"
                + "job_description VARCHAR(25),"
                + "acad_qualification VARCHAR(25),"
                + "date_of_birth VARCHAR(125),"
                + "imagepath VARCHAR(125)," 
                + "nok_fullname VARCHAR(150),"
                + "nok_address VARCHAR(100),"
                + "nok_email VARCHAR(50),"
                + "nok_number VARCHAR(50),"               
                + "st_created_on DATE DEFAULT CURRENT_DATE )";
         
         String createAttendanceSQL = "CREATE TABLE attendance ("
                + "attendance_id INTEGER NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
                + "staff_id INTEGER NOT NULL,"
                + "date VARCHAR(150),"
                + "time_in TIME DEFAULT CURRENT_TIME,"
                + "time_out VARCHAR(150)  )";
         
         try {
            
            if(!checkIfTableExist(connection, "admin")){
                try {
                    prepStatement = connection.prepareStatement(createAdminSQL);
                    if(prepStatement.execute()){ };
                } catch (SQLException ex) {
                    System.out.println("SQL ECEPTION : " + ex.getMessage());
                    close();
                }
            }else{ System.out.println("admin table ready and set for action"); }
            
            if(!checkIfTableExist(connection, "staffs")){
                try {
                    prepStatement = connection.prepareStatement(createStaffSQL);
                    prepStatement.execute();
                } catch (SQLException ex) {
                    System.out.println("SQL ECEPTION : " + ex.getMessage());
                    close();
                }
            }else{ System.out.println("staffs table ready and set for action"); }
            
            if(!checkIfTableExist(connection, "attendance")){
                try {
                    prepStatement = connection.prepareStatement(createAttendanceSQL);
                    prepStatement.execute();
                } catch (SQLException ex) {
                    System.out.println("SQL ECEPTION : " + ex.getMessage());
                    close();
                }
            }else{ System.out.println("attendance table ready and set for action"); }
            
                        
        } catch (SQLException ex) {
            System.out.println("Failed to initialize the database tables");
            Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
            close();
            shutDownDatabase();//becos it failed to initialize the database
        }
        
      }// End initDb
    

/**
 * add a staff to the database
 * 
 * @param staffModel
 * @return the rowId of the inserted data
 */
    public int addStaff(StaffModel staffModel){
        int insertedRow = 0;
        int insertedId = 0;
        
        String query = "INSERT INTO staffs "
                    + "(fullname , fingerprint, address, number, email, blood_group, job_description,"
                + "acad_qualification, date_of_birth, imagepath , nok_fullname, nok_address, nok_email, nok_number)"
                    + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        try{
            
           
            prepStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            
            prepStatement.setString(1, staffModel.getStaffFullname());
            prepStatement.setString(2, staffModel.getFingerPrint());
            prepStatement.setString(3, staffModel.getStaffAddress());
            prepStatement.setString(4, staffModel.getNumber());
            prepStatement.setString(5, staffModel.getStaffEmail());
            prepStatement.setString(6, staffModel.getBloodGrp());
            prepStatement.setString(7, staffModel.getJobDescrptn());
            prepStatement.setString(8, staffModel.getAcadQual());
            prepStatement.setString(9, staffModel.getdOfBirth());
            prepStatement.setString(10, staffModel.getImagepath());
            prepStatement.setString(11, staffModel.getNokFullname());
            prepStatement.setString(12, staffModel.getNokAddress());
            prepStatement.setString(13, staffModel.getNokEmail());
            prepStatement.setString(14, staffModel.getNokPhnumber());

            
            insertedRow = prepStatement.executeUpdate();
            if(insertedRow > 0){

                ResultSet resultSet = prepStatement.getGeneratedKeys();
                if(resultSet.next()){
                    insertedId = (int) resultSet.getLong(1);

                }
            
            }
        } catch(SQLException ex){
             System.out.println("error inserting data ... " + ex.getMessage());
             ex.printStackTrace();
        }
        
        return insertedId;
    }    
    
    
/**
 * inserts a staff's details if it doesnt exist
 * @param model
 * @return -1 if the staff exist 
 */
    public int insertStaffIfNotExist(StaffModel model){
        int insertedRow = 0;
        int insertedId = 0;
        
        try {
            
            //first check if it exist
            prepStatement = connection.prepareStatement("SELECT staff_id FROM staffs WHERE fullname = ? ");
            prepStatement.setString(1, model.getStaffFullname());
            
            resultSet = prepStatement.executeQuery();
            
            if(resultSet.next()){
                //record found
                System.out.println("found record with id : " + resultSet.getInt("staff_id"));
                insertedId = -1;
            }else{
                //try to add student record not found
                insertedId = addStaff(model);
            }
        } catch (SQLException ex) {
            System.out.println("error inserting data ... " + ex.getMessage());
            ex.printStackTrace();
        }
        
        return insertedId;
       
    }
    
    /**
     * updates a staff record
     * @param model
     * @return the row updated
     */
    public int updateStaff(StaffModel staffModel){        
        
        int updatedRow = 0;
        try{
                String query =  " UPDATE staffs SET " + 
                                " fullname = ?, fingerprint = ?, address = ?, "
                        + " number = ?, email = ?, blood_group = ?, job_description = ?, "
                        + "acad_qualification = ?, date_of_birth = ?, imagepath = ?, nok_fullname  = ?,"
                        + "nok_address = ?, nok_email = ?, nok_number = ?"
                        + "WHERE staff_id = ? ";

                prepStatement = connection.prepareStatement(query);
                prepStatement.setString(1, staffModel.getStaffFullname());
                prepStatement.setString(2, staffModel.getFingerPrint());
                prepStatement.setString(3, staffModel.getStaffAddress());
                prepStatement.setString(4, staffModel.getNumber());
                prepStatement.setString(5, staffModel.getStaffEmail());
                prepStatement.setString(6, staffModel.getBloodGrp());
                prepStatement.setString(7, staffModel.getJobDescrptn());
                prepStatement.setString(8, staffModel.getAcadQual());
                prepStatement.setString(9, staffModel.getdOfBirth());
                prepStatement.setString(10, staffModel.getImagepath());
                prepStatement.setString(11, staffModel.getNokFullname());
                prepStatement.setString(12, staffModel.getNokAddress());
                prepStatement.setString(13, staffModel.getNokEmail());
                prepStatement.setString(14, staffModel.getNokPhnumber());
                prepStatement.setInt(15, staffModel.getStaffId());

                updatedRow = prepStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return updatedRow;
    }
    
    /**
     * get the id of a staff
     * @param name
     * @return 
     */
    public int getStaffId(String name){
         String query = "SELECT staff_id FROM staffs "
                + "WHERE fullname = ? ";        
        int result = -1;
        try {
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1, name);
            resultSet = prepStatement.executeQuery();
            if(resultSet.next()){
                result = resultSet.getInt("staff_id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    
    /**
     * deletes a staff with the fullname
     * @param name
     * @return 
     */
    public int deleteStaff(String name){
        int id = getStaffId(name);// get the id of the staff
        
        String query = "DELETE FROM staffs "
                + "WHERE fullname = ? ";
        String s_query = "DELETE FROM attendance "
                + "WHERE staff_id = ? ";
        String a_query = "DELETE FROM admin "
                + "WHERE staff_id = ? ";
        int result = -1;
        try {
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1, name);
            result = prepStatement.executeUpdate();
            
            //Delete the records from attendance table if it exist
            prepStatement = connection.prepareStatement(s_query);
            prepStatement.setInt(1, id);
            prepStatement.executeUpdate();
            
            //Delete the records from admin table if it exist
            prepStatement = connection.prepareStatement(a_query);
            prepStatement.setInt(1, id);
            prepStatement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    /**
     * get the  staff details by the 
     * @param fullname
     * @return staff model
     */
    public StaffModel getStaffDetails(String fullname){
        StaffModel model = null;
        try{
            String query = "SELECT * FROM staffs WHERE fullname = ?";
            
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1, fullname);
            
            resultSet = prepStatement.executeQuery();
            if(resultSet.next()){
                model = new StaffModel(resultSet.getString("fullname"),resultSet.getString("address"), 
                        resultSet.getString("number"), resultSet.getString("email"), resultSet.getString("fingerprint"));
                model.setStaffId(resultSet.getInt("staff_id"));
                model.setBloodGrp(resultSet.getString("blood_group"));
                model.setJobDescrptn(resultSet.getString("job_description"));   
                model.setAcadQual(resultSet.getString("acad_qualification"));
                model.setdOfBirth(resultSet.getString("date_of_birth"));
                model.setImagepath(resultSet.getString("imagepath"));
                model.setNokFullname(resultSet.getString("nok_fullname"));
                model.setNokAddress(resultSet.getString("nok_address"));
                model.setNokEmail(resultSet.getString("nok_email"));
                model.setNokPhnumber(resultSet.getString("nok_number"));
                model.setBloodGrp(resultSet.getString("blood_group"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return model;
    }
    
    /**
     * Used for searching for a staff with name
     * @param query the query string
     * @return List of staff as object
     * @throws SQLException 
     */
    public List<StaffModel> searchStaff(String query) {
        List<StaffModel> stafflist = new ArrayList<>();
        try {
            /* TODO: make the query a little bit smarter by ref we-elect search SQL
            query */
            prepStatement = connection.prepareStatement("SELECT fullname FROM staffs WHERE lower(fullname) like '%'||?||'%' OR fullname LIKE ?||'%' ORDER BY fullname ASC ");
            prepStatement.setString(1, query);
            prepStatement.setString(2, query);
            
            resultSet = prepStatement.executeQuery();
            
            
            while (resultSet.next()) {
                
                StaffModel model = new StaffModel(resultSet.getString("fullname"),null, 
                        null, null, null);
                
                stafflist.add(model);
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return stafflist;
    }
    
    public String[][] getAllStaffDetails(){
        
        ArrayList<String[]> singleStaffdata = new ArrayList();
        String newdata [][] = null;
        
        List<StaffModel> staffList = new ArrayList<>();
        try{
            prepStatement = connection.prepareStatement("SELECT * FROM staffs ORDER BY fullname ASC ");
            resultSet = prepStatement.executeQuery();       
            int serial = 1;
            while(resultSet.next()){
                
                String  ddata[] = {String.valueOf(serial),resultSet.getString("fullname"), resultSet.getString("address"), 
                                   resultSet.getString("number"), resultSet.getString("email")};
                                
                singleStaffdata.add(ddata);                 
               serial++;
            }   
            newdata = new String[ singleStaffdata.size()][5];
            for(int j = 0; j < singleStaffdata.size(); j++){
                newdata[j] = singleStaffdata.get(j);            
            }
            singleStaffdata.clear();
        
        }catch(SQLException e){
            System.out.println("Sql exceprioin : " + e.getMessage());
        }
        finally{
            try{
                resultSet.close();
            }catch(SQLException ex){
                ex.printStackTrace();
                close();
            }catch(NullPointerException e){
                System.out.println("Error loading image : " + e.getMessage());
            }
        }    
        return newdata;
        
    }
    
    public int getTotalWorkers(){
        int totalWorkers = 0;
        try{
            String query = "SELECT COUNT(staff_id)AS count FROM staffs";
            prepStatement = connection.prepareStatement(query);
            resultSet = prepStatement.executeQuery();
            if(resultSet.next()){
                totalWorkers = resultSet.getInt("count");
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return totalWorkers;
    }
    
    /**
     * get the record for each day
     * @param date
     * @return 
     */
    public String[][] getDailyAttendanceRecords(String date){
        
        ArrayList<String[]> singleAttendanceData = new ArrayList();
        String newdata [][] = null;
        
        List<AttendanceModel> attendanceList = new ArrayList<>();
        try{
            String query = "SELECT a.attendance_id, s.staff_id, s.fullname, "
                    + "a.date, a.time_in, a.time_out "
                    + "FROM attendance As a, staffs As s "
                    + "WHERE a.date = ? AND a.staff_id = s.staff_id  ORDER BY s.fullname ASC ";
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1, date);
            resultSet = prepStatement.executeQuery();       
            int serial = 1;
            while(resultSet.next()){
                
                String  ddata[] = {String.valueOf(serial),resultSet.getString("fullname"), resultSet.getString("date"), 
                                   resultSet.getString("time_in"), resultSet.getString("time_out")};
                                
                singleAttendanceData.add(ddata);                 
               serial++;
            }   
            newdata = new String[ singleAttendanceData.size()][5];
            for(int j = 0; j < singleAttendanceData.size(); j++){
                newdata[j] = singleAttendanceData.get(j);            
            }
            singleAttendanceData.clear();
        
        }catch(SQLException e){
            System.out.println("Sql exceprioin : " + e.getMessage());
        }
        finally{
            try{
                resultSet.close();
            }catch(SQLException ex){
                ex.printStackTrace();
                close();
            }catch(NullPointerException e){
                System.out.println("Error loading image : " + e.getMessage());
            }
        }    
        return newdata;
        
    }
    
    
     /**
     * get the record for each day
     * 
     * @return array of attendance records
     */
    public String[][] getDailyAttendanceRecords(){
        
        ArrayList<String[]> singleAttendanceData = new ArrayList();
        String newdata [][] = null;
        
        List<AttendanceModel> attendanceList = new ArrayList<>();
        try{
            String query = "SELECT a.attendance_id, s.staff_id, s.fullname, "
                    + "a.date, a.time_in, a.time_out "
                    + "FROM attendance As a, staffs As s "
                    + "WHERE a.staff_id = s.staff_id  ORDER BY a.date DESC ";
            prepStatement = connection.prepareStatement(query);
            
            resultSet = prepStatement.executeQuery();       
            int serial = 1;
            while(resultSet.next()){
                
                String  ddata[] = {String.valueOf(serial),resultSet.getString("fullname"), resultSet.getString("date"), 
                                   resultSet.getString("time_in"), resultSet.getString("time_out")};
                                
                singleAttendanceData.add(ddata);                 
               serial++;
            }   
            newdata = new String[ singleAttendanceData.size()][5];
            for(int j = 0; j < singleAttendanceData.size(); j++){
                newdata[j] = singleAttendanceData.get(j);            
            }
            singleAttendanceData.clear();
        
        }catch(SQLException e){
            System.out.println("Sql exceprioin : " + e.getMessage());
        }
        finally{
            try{
                resultSet.close();
            }catch(SQLException ex){
                ex.printStackTrace();
                close();
            }catch(NullPointerException e){
                System.out.println("Error loading image : " + e.getMessage());
            }
        }    
        return newdata;
        
    }
    
    /**
     * get attendance by filter
     * @param date1
     * @param date2
     * @return array of attendance records
     */
    public String[][] getAttendanceRecordsFromDate(String date1, String date2){
        
        ArrayList<String[]> singleAttendanceData = new ArrayList();
        String newdata [][] = null;
        
        List<AttendanceModel> attendanceList = new ArrayList<>();
        try{
            String query = "SELECT a.attendance_id, s.staff_id, s.fullname, "
                    + "a.date, a.time_in, a.time_out "
                    + "FROM attendance As a, staffs As s "
                    + "WHERE a.staff_id = s.staff_id AND a.date BETWEEN ? AND ?  ORDER BY a.date ASC ";
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1, date1);
            prepStatement.setString(2, date2);
            resultSet = prepStatement.executeQuery();       
            int serial = 1;
            while(resultSet.next()){
                
                String  ddata[] = {String.valueOf(serial),resultSet.getString("fullname"), resultSet.getString("date"), 
                                   resultSet.getString("time_in"), resultSet.getString("time_out")};
                                
                singleAttendanceData.add(ddata);                 
               serial++;
            }   
            newdata = new String[ singleAttendanceData.size()][5];
            for(int j = 0; j < singleAttendanceData.size(); j++){
                newdata[j] = singleAttendanceData.get(j);            
            }
            singleAttendanceData.clear();
        
        }catch(SQLException e){
            System.out.println("Sql exceprioin : " + e.getMessage());
        }
        finally{
            try{
                resultSet.close();
            }catch(SQLException ex){
                ex.printStackTrace();
                close();
            }catch(NullPointerException e){
                System.out.println("Error loading image : " + e.getMessage());
            }
        }    
        return newdata;
      }
    
     /**
     * get the attendance records for a staff
     * @param staffId
     * @return 
     */
    public String[][] getStaffAttendancePerDay(int staffId){
        ArrayList<String[]> singleAttendanceData = new ArrayList();
        String newdata [][] = null;
         try{
            String query = "SELECT attendance_id, staff_id, "
                    + "date, time_in, time_out "
                    + "FROM attendance "
                    + "WHERE staff_id = ?  ORDER BY date ASC ";
            prepStatement = connection.prepareStatement(query);
            prepStatement.setInt(1, staffId);
            resultSet = prepStatement.executeQuery();       
            int serial = 1;
            while(resultSet.next()){
                
                String  ddata[] = {String.valueOf(serial),resultSet.getString("date"), 
                                   resultSet.getString("time_in"), resultSet.getString("time_out")};
                                
                singleAttendanceData.add(ddata);                 
               serial++;
            }   
            newdata = new String[ singleAttendanceData.size()][4];
            for(int j = 0; j < singleAttendanceData.size(); j++){
                newdata[j] = singleAttendanceData.get(j);            
            }
            singleAttendanceData.clear();
        
        }catch(SQLException e){
            System.out.println("Sql exceprioin : " + e.getMessage());
        }
        finally{
            try{
                resultSet.close();
            }catch(SQLException ex){
                ex.printStackTrace();
                close();
            }catch(NullPointerException e){
                System.out.println("Error loading image : " + e.getMessage());
            }
        }    
        return newdata;
    }
    
     /**
     * get the time-in record for each day to be displayed at the dashboard
     * @param date
     * @return 
     */
    public String[][] getDashboardAttendanceRecords(String date){
        
        ArrayList<String[]> singleAttendanceData = new ArrayList();
        String newdata [][] = null;
        
        List<AttendanceModel> attendanceList = new ArrayList<>();
        try{
            String query = "SELECT a.attendance_id, s.staff_id, s.fullname, "
                    + "a.date, a.time_in, a.time_out "
                    + "FROM attendance As a, staffs As s "
                    + "WHERE a.date = ? AND a.staff_id = s.staff_id  ORDER BY s.fullname ASC ";
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1, date);
            resultSet = prepStatement.executeQuery();       
            int serial = 1;
            while(resultSet.next()){
                
                String  ddata[] = {String.valueOf(serial),resultSet.getString("fullname"), 
                                   resultSet.getString("time_in")};
                                
                singleAttendanceData.add(ddata);                 
               serial++;
            }   
            newdata = new String[ singleAttendanceData.size()][3];
            for(int j = 0; j < singleAttendanceData.size(); j++){
                newdata[j] = singleAttendanceData.get(j);            
            }
            singleAttendanceData.clear();
        
        }catch(SQLException e){
            System.out.println("Sql exceprioin : " + e.getMessage());
        }
        finally{
            try{
                resultSet.close();
            }catch(SQLException ex){
                ex.printStackTrace();
                close();
            }catch(NullPointerException e){
                System.out.println("Error loading image : " + e.getMessage());
            }
        }    
        return newdata;
        
    }
    
    /**
     * inserts the time for each staff
     * @param model
     * @return 
     */
    public int markAttendance(AttendanceModel model, boolean isTimeIn){
        int result = 0;
        
        try{
            String query = "INSERT INTO attendance "
                    + "(staff_id, date) VALUES (?, ?)";
            String s_query = " UPDATE attendance SET "
                    + " time_out = ? WHERE staff_id = ?  AND date = ? ";// OR USE DATE()----
            
            if(isTimeIn){
                prepStatement = connection.prepareStatement(query);                
                prepStatement.setInt(1, model.getStaffId());
                prepStatement.setString(2, model.getDate());
                result = prepStatement.executeUpdate();
            }
            else{
                prepStatement = connection.prepareStatement(s_query);
                prepStatement.setString(1, model.getTimeOut());
                prepStatement.setInt(2, model.getStaffId());
                prepStatement.setString(3, model.getDate());
                result = prepStatement.executeUpdate();
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        
        return result;
    }
    
    
   
    
    /**
     * insert an admin 
     * @param model
     * @return 
     */
    public int addAdmin(AdminModel model){
        int insertedRow = 0;
        try{
            // PreparedStatements can use variables and are more efficient
            prepStatement = connection.prepareStatement("insert into admin (staff_id, fingerprint, password ) values ( ?, ?, ?)");
            prepStatement.setInt(1, model.getStaffId());
            prepStatement.setString(2, model.getFingerStream());            
            prepStatement.setString(3, model.getPassword());
            

            insertedRow = prepStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("error inserting data ... " + ex.getMessage());
        }
        
        
        return insertedRow;
    }
    
    public List<AdminModel> getAdmin(){
        List<AdminModel> modelList = new ArrayList<>();
        
        try{
            prepStatement = connection.prepareStatement("SELECT * FROM admin");
            resultSet = prepStatement.executeQuery();
            
            while(resultSet.next()){
                 
              AdminModel  model = new AdminModel(resultSet.getInt("staff_id"), resultSet.getString("password"), resultSet.getString("fingerPrint"));
              modelList.add(model);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        
        return modelList;
    }
    
    /**
     * get if a staff is an admin
     * @param id
     * @return adminModel
     */
    public AdminModel getAdminWithId(int id){
        AdminModel model = null;
        
        try{
            prepStatement = connection.prepareStatement("SELECT * FROM admin WHERE staff_id = ?");
            prepStatement.setInt(1, id);
            resultSet = prepStatement.executeQuery();
            
            if(resultSet.next()){
                 
                model = new AdminModel(resultSet.getInt("staff_id"), resultSet.getString("password"), resultSet.getString("fingerPrint"));
              
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        
        return model;
    }
    
    /**
     * update an admin password
     * @param model
     * @return 
     */
    public int updateAdmin(AdminModel model){
        int result = -1;
        String query ="UPDATE admin set password = ? WHERE staff_id = ?";
        try{
            prepStatement = connection.prepareStatement(query);
            prepStatement.setString(1, model.getPassword());;
            prepStatement.setInt(2, model.getStaffId());
            result = prepStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    /**
     * get the attendance dates with the total numbers of staff for the date
     * @return attendance list
     */
    public List<AttendanceModel> getDailyAttendance(){
        List<AttendanceModel> modelList = new ArrayList<>();
        String query = "SELECT date, COUNT(staff_id) as total FROM attendance GROUP BY date ORDER BY date";
        try{
            prepStatement = connection.prepareStatement(query);
            resultSet = prepStatement.executeQuery();
            while(resultSet.next()){
                AttendanceModel model= new AttendanceModel();
                model.setDate(resultSet.getString("date"));
                model.setDailyAttendance(resultSet.getInt("total"));
                modelList.add(model);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return modelList;
    }
    
    /**
     * deletes a admin 
     * @param staff_id 
     * @return 
     */
    public int deleteAdmin(int staff_id){        
        
        String query = "DELETE FROM admin "
                + "WHERE staff_id = ? ";
        int result = -1;
        try {
            prepStatement = connection.prepareStatement(query);
            prepStatement.setInt(1, staff_id);
            result = prepStatement.executeUpdate();            
           
            
        } catch (SQLException ex) {
            Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    
     /**
     * use to load all fingerprint of the student in other to validate them against each one 
     * @return List of Finger object
     * @throws SQLException 
     */
    public List<FingerModel> getAllFingers() {
        
        List<FingerModel> fingers = null;
        try {
            prepStatement = connection.prepareStatement("select staff_id, fullname, fingerprint from staffs");
            resultSet = prepStatement.executeQuery();
            
            fingers = writeStream(resultSet);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(DbHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fingers;
    }
    
    /**
     * used to iterate over the sets of fingers result set 
     * @param resultSet
     * @return List of Finger object
     * @throws SQLException 
     */
    private List<FingerModel> writeStream(ResultSet resultSet) throws SQLException{
        List<FingerModel> fingerPrints = new ArrayList<>();
        // ResultSet is initially before the first data set
        while (resultSet.next()) {
            // It is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g. resultSet.getSTring(2);
            int id = resultSet.getInt("staff_id");
            String fullname = resultSet.getString("fullname");
            String fingerprint = resultSet.getString("fingerprint");
            
            
            FingerModel finger = new FingerModel(id, fingerprint, fullname);
            fingerPrints.add(finger);
        }
        
        return fingerPrints;
    }
    
    
}



























