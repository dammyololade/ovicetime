/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author user
 */
public class FxGraph {
     JFXPanel fxPanel;
     int incomingYAxis[];
     int incomingXAxis[];
    public FxGraph() {
       fxPanel = new JFXPanel();
       fxPanel.setSize(400, 350);
       initFxComponents();
    }
     
 /**
  * A method to create a graph using JFX
  * @return JFXpanel containing the drawn graph
  */
    public JFXPanel initFxComponents(){

        Platform.runLater(new Runnable() {
          @Override
          public void run() {
             //Defining the x axis  
            BorderPane graphPane = new BorderPane();
            graphPane.prefWidth(350);
            graphPane.prefHeight(350);
            NumberAxis xAxis = new NumberAxis(1960, 2020, 10); 
            xAxis.setLabel("Days"); 

            //Defining the y axis   
            NumberAxis yAxis = new NumberAxis   (0, 350, 50); 
           // incomingYAxis.setLabel("No.of schools"); 

            //Creating the line chart 
           // LineChart linechart = new LineChart(xAxis, incomingYAxis);  
            AreaChart<Number, Number> areaChart = new AreaChart(xAxis, yAxis);
            areaChart.setPrefSize(350, 350);
              
            //Prepare XYChart.Series objects by setting data 
            XYChart.Series series = new XYChart.Series(); 
            series.setName("Staff Attendance Per Day"); 
            
             
//            series.getData().add(new XYChart.Data(1970, 100)); 
//            series.getData().add(new XYChart.Data(1980, 200)); 
//            series.getData().add(new XYChart.Data(1990, 300)); 
//            series.getData().add(new XYChart.Data(2000, 300)); 
//            series.getData().add(new XYChart.Data(2013, 100)); 
//            series.getData().add(new XYChart.Data(2017, 200)); 
            for(int i = 0; i< incomingXAxis.length; i++){
                
                series.getData().add(new XYChart.Data(incomingXAxis[i], incomingYAxis[i]));
            }
            //Setting the data to Line chart    
           // linechart.getData().add(series);  
           
            areaChart.getData().add(series); 
            graphPane.setCenter(areaChart);
           // Group root = new Group(areaChart); 

            //Creating a scene object 
            Scene scene = new Scene(graphPane, 350, 350);  


              fxPanel.setScene(scene);
        }
      });
        return fxPanel;
     }
    
    public void setGraphDetails(int[] xAxis, int[] yAxis){
        this.incomingXAxis = xAxis;
        this.incomingYAxis = yAxis;
    }
    
}
