/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime;


import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import oviceTime.dialogs.AboutUsDialog;
import oviceTime.dialogs.AlertDialog;
import oviceTime.listeners.OnMailSentListener;

/**
 *
 * @author WeaverBird
 */
public class DropDownMenuPanel extends javax.swing.JPanel implements OnMailSentListener{
    
    JFrame windowFrame;
    

    /**
     * Creates new form DropDownMenuPanel
     */
    public DropDownMenuPanel(JFrame parent) {
        initComponents();
        
//        windowFrame = (JFrame) SwingUtilities.getWindowAncestor(DropDownMenuPanel.this);
          windowFrame = parent;
    }
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        feedBackBtn = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        aboutUsBtn = new javax.swing.JButton();
        ExitButton = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        exportButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setToolTipText("");
        setPreferredSize(new java.awt.Dimension(130, 220));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204), 0));
        jPanel1.setPreferredSize(new java.awt.Dimension(130, 220));

        feedBackBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/comment-processing-outline_18.png"))); // NOI18N
        feedBackBtn.setText("Feedback");
        feedBackBtn.setContentAreaFilled(false);
        feedBackBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        feedBackBtn.setIconTextGap(10);
        feedBackBtn.setMargin(new java.awt.Insets(2, 8, 2, 8));
        feedBackBtn.setMaximumSize(new java.awt.Dimension(95, 32));
        feedBackBtn.setMinimumSize(new java.awt.Dimension(95, 32));
        feedBackBtn.setPreferredSize(new java.awt.Dimension(95, 32));
        feedBackBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                feedBackBtnActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/screwdriver_18.png"))); // NOI18N
        jButton3.setText("Get Support");
        jButton3.setContentAreaFilled(false);
        jButton3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton3.setIconTextGap(10);
        jButton3.setMargin(new java.awt.Insets(2, 8, 2, 8));
        jButton3.setMaximumSize(new java.awt.Dimension(107, 32));
        jButton3.setMinimumSize(new java.awt.Dimension(107, 32));
        jButton3.setPreferredSize(new java.awt.Dimension(107, 32));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        aboutUsBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/fingerprint_18.png"))); // NOI18N
        aboutUsBtn.setText("ABout");
        aboutUsBtn.setContentAreaFilled(false);
        aboutUsBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        aboutUsBtn.setIconTextGap(10);
        aboutUsBtn.setMargin(new java.awt.Insets(2, 8, 2, 8));
        aboutUsBtn.setMaximumSize(new java.awt.Dimension(77, 32));
        aboutUsBtn.setMinimumSize(new java.awt.Dimension(77, 32));
        aboutUsBtn.setPreferredSize(new java.awt.Dimension(77, 32));
        aboutUsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutUsBtnActionPerformed(evt);
            }
        });

        ExitButton.setBackground(new java.awt.Color(255, 255, 255));
        ExitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/power.png"))); // NOI18N
        ExitButton.setText("Exit");
        ExitButton.setContentAreaFilled(false);
        ExitButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ExitButton.setIconTextGap(10);
        ExitButton.setMargin(new java.awt.Insets(2, 8, 2, 8));
        ExitButton.setPreferredSize(new java.awt.Dimension(77, 32));
        ExitButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ExitButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ExitButtonMouseExited(evt);
            }
        });
        ExitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitButtonActionPerformed(evt);
            }
        });

        jButton5.setBackground(new java.awt.Color(255, 255, 255));
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/settings_18.png"))); // NOI18N
        jButton5.setText("Import");
        jButton5.setBorderPainted(false);
        jButton5.setContentAreaFilled(false);
        jButton5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton5.setIconTextGap(10);
        jButton5.setMargin(new java.awt.Insets(2, 8, 2, 8));
        jButton5.setMaximumSize(new java.awt.Dimension(87, 32));
        jButton5.setPreferredSize(new java.awt.Dimension(87, 32));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        exportButton.setBackground(new java.awt.Color(255, 255, 255));
        exportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/settings_18.png"))); // NOI18N
        exportButton.setText("Export");
        exportButton.setBorderPainted(false);
        exportButton.setContentAreaFilled(false);
        exportButton.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        exportButton.setIconTextGap(10);
        exportButton.setMargin(new java.awt.Insets(2, 8, 2, 8));
        exportButton.setMaximumSize(new java.awt.Dimension(87, 32));
        exportButton.setPreferredSize(new java.awt.Dimension(87, 32));
        exportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportButtonActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/settings_18.png"))); // NOI18N
        jButton1.setText("Settings");
        jButton1.setBorderPainted(false);
        jButton1.setContentAreaFilled(false);
        jButton1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton1.setIconTextGap(10);
        jButton1.setMargin(new java.awt.Insets(2, 8, 2, 8));
        jButton1.setMaximumSize(new java.awt.Dimension(87, 32));
        jButton1.setPreferredSize(new java.awt.Dimension(87, 32));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ExitButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButton5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(feedBackBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
            .addComponent(aboutUsBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(exportButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(exportButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(feedBackBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(aboutUsBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(ExitButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void aboutUsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutUsBtnActionPerformed
        //Show about us dialog
        AboutUsDialog dialog = new AboutUsDialog(windowFrame, true);
        dialog.setLocationRelativeTo(windowFrame);
        dialog.setVisible(true);
    }//GEN-LAST:event_aboutUsBtnActionPerformed

    private void ExitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExitButtonActionPerformed
        try {
            // TODO add your handling code here:
           System.exit(0);
           
            //windowFrame.dispose();
        } catch (Throwable ex) {
            Logger.getLogger(DropDownMenuPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ExitButtonActionPerformed

    private void ExitButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ExitButtonMouseEntered
        // TODO add your handling code here:
        ExitButton.setIcon(new ImageIcon(getClass().getResource(
                             "/oviceTime/icon/" + "power-2" + ".png")));
    }//GEN-LAST:event_ExitButtonMouseEntered

    private void ExitButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ExitButtonMouseExited
        // TODO add your handling code here:
        ExitButton.setIcon(new ImageIcon(getClass().getResource(
                             "/oviceTime/icon/" + "power" + ".png")));
  
    }//GEN-LAST:event_ExitButtonMouseExited

    private void feedBackBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_feedBackBtnActionPerformed
        // TODO add your handling code here:
        MailForm form = new MailForm();
        form.setReceiverEmail("ovTime@gmail.com");
        form.setEmailSentListener(this);
        form.setVisible(true);
    }//GEN-LAST:event_feedBackBtnActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        
//        ImportDialog iDialog = new ImportDialog(windowFrame, true);
//        iDialog.setVisible(true);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed

    private void exportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportButtonActionPerformed
        // TODO add your handling code here:
//        ExportDialog eDialog = new ExportDialog(windowFrame, true);
//        eDialog.setVisible(true);
    }//GEN-LAST:event_exportButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ExitButton;
    private javax.swing.JButton aboutUsBtn;
    private javax.swing.JButton exportButton;
    private javax.swing.JButton feedBackBtn;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onMailSent() {
        AlertDialog dialog = new AlertDialog(windowFrame, true);
        dialog.setMessage("YOur feedback has been sent");
        dialog.setVisible(true);
        
       
    }
}
