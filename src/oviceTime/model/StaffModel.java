/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.model;

/**
 *
 * @author user
 */
public class StaffModel {
    
    
    private int staffId;
    private String staffFullname;
    private String staffAddress;
    private String staffPhNumber;
    private String staffEmail;
    private String fingerPrint;
    private String nokFullname, nokAddress, nokEmail, nokPhnumber, bloodGrp, jobDescrptn, acadQual, dOfBirth, imagepath;

    public StaffModel(String fullname, String address, String number, String email, String fingerPrint) {
        
        this.staffFullname = fullname;
        this.staffAddress = address;
        this.staffPhNumber = number;
        this.staffEmail = email;
        this.fingerPrint = fingerPrint;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getStaffFullname() {
        return staffFullname;
    }

    public void setStaffFullname(String staffFullname) {
        this.staffFullname = staffFullname;
    }

    public String getStaffAddress() {
        return staffAddress;
    }

    public void setStaffAddress(String staffAddress) {
        this.staffAddress = staffAddress;
    }

    public String getNumber() {
        return staffPhNumber;
    }

    public void setNumber(String number) {
        this.staffPhNumber = number;
    }

    public String getStaffEmail() {
        return staffEmail;
    }

    public void setStaffEmail(String staffEmail) {
        this.staffEmail = staffEmail;
    }

    public String getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(String fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    public String getStaffPhNumber() {
        return staffPhNumber;
    }

    public void setStaffPhNumber(String staffPhNumber) {
        this.staffPhNumber = staffPhNumber;
    }

    public String getNokFullname() {
        return nokFullname;
    }

    public void setNokFullname(String nokFullname) {
        this.nokFullname = nokFullname;
    }

    public String getNokAddress() {
        return nokAddress;
    }

    public void setNokAddress(String nokAddress) {
        this.nokAddress = nokAddress;
    }

    public String getNokEmail() {
        return nokEmail;
    }

    public void setNokEmail(String nokEmail) {
        this.nokEmail = nokEmail;
    }

    public String getNokPhnumber() {
        return nokPhnumber;
    }

    public void setNokPhnumber(String nokPhnumber) {
        this.nokPhnumber = nokPhnumber;
    }

    public String getBloodGrp() {
        return bloodGrp;
    }

    public void setBloodGrp(String bloodGrp) {
        this.bloodGrp = bloodGrp;
    }

    public String getJobDescrptn() {
        return jobDescrptn;
    }

    public void setJobDescrptn(String jobDescrptn) {
        this.jobDescrptn = jobDescrptn;
    }

    public String getAcadQual() {
        return acadQual;
    }

    public void setAcadQual(String acadQual) {
        this.acadQual = acadQual;
    }

    public String getdOfBirth() {
        return dOfBirth;
    }

    public void setdOfBirth(String dOfBirth) {
        this.dOfBirth = dOfBirth;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }
    
    
}
