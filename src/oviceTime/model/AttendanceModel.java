/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.model;

import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author user
 */
public class AttendanceModel {
    
    private int attendanceId;
    private int staffId;
    private String date, timeOut, staffName;
    private Time timeIn ;
    private int dailyAttendance;

    public AttendanceModel() {
    }
    
    

    public AttendanceModel(int attendanceId, int staffId, String date, Time timeIn, String timeOut) {
        this.attendanceId = attendanceId;
        this.staffId = staffId;
        this.date = date;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
    }

    public int getAttendanceId() {
        return attendanceId;
    }

    public void setAttendanceId(int attendanceId) {
        this.attendanceId = attendanceId;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Time getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(Time timeIn) {
        this.timeIn = timeIn;
    }

    public String getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public int getDailyAttendance() {
        return dailyAttendance;
    }

    public void setDailyAttendance(int dailyAttendance) {
        this.dailyAttendance = dailyAttendance;
    }
    
    
    
}
