/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.model;

/**
 *
 * @author user
 */
public class AdminModel {    
    
  
    private String password;
    private String fingerStream; 
    private int staffId;

    public AdminModel(int staffId, String password, String fingerStream) {
        this.staffId = staffId;        
        this.password = password;
        this.fingerStream = fingerStream;
    }

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFingerStream() {
        return fingerStream;
    }

    public void setFingerStream(String fingerStream) {
        this.fingerStream = fingerStream;
    }
    
    
    
}
