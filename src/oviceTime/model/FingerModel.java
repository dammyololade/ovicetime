/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.model;

import java.io.InputStream;

/**
 *
 * @author TK
 */
public class FingerModel {
    
     private int staffId;
    private String fullname;
    private String fingers;

    public FingerModel(int id, String fingers, String fullname) {
        this.staffId = id;
        this.fingers = fingers;
        this.fullname = fullname;
    }

    public int getStaffId() {
        return staffId;
    }
    public String getfullname(){
        return fullname;
    }

    public String getFinger() {
        return fingers;
    }
    
    
}
