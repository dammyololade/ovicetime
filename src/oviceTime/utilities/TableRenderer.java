/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.utilities;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author dammyololade
 */
public class TableRenderer extends DefaultTableCellRenderer{
    
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
      boolean hasFocus, int row, int column) {
        
        //Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        setEnabled(table == null || table.isEnabled());  
    
       if ((row % 2) == 0)
            setBackground(Color.white);
        else
            setBackground(new Color(240, 240, 240));

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        return this;
   
  }
    
}
