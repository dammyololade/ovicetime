package oviceTime.utilities;

import com.digitalpersona.onetouch.DPFPCaptureFeedback;
import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.event.DPFPDataAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPDataEvent;
import com.digitalpersona.onetouch.capture.event.DPFPImageQualityAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPImageQualityEvent;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusEvent;
import com.digitalpersona.onetouch.capture.event.DPFPSensorAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPSensorEvent;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import com.digitalpersona.onetouch.verification.DPFPVerificationResult;
import oviceTime.AdminRegistration;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import oviceTime.dialogs.Registration;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * Created by Adeyemo Adedamola on 20/09/2017.
 */
public class DPFPProvider {

    private DPFPCapture capturer;
    public DPFPEnrollment enroller;
    public DPFPVerification verificator;
    public static boolean readerStatus;
    
    private ProviderListener pListener;
    
    public boolean isFPConnected = false;
    public boolean isFPSensorReady = false;
    public boolean isFPDataReady = false;
    public boolean isFPImageQualityGood = false;
    
    DPFPTemplate template = null;

    public DPFPProvider(Object mClass) {
        this.capturer = DPFPGlobal.getCaptureFactory().createCapture();
        enroller = DPFPGlobal.getEnrollmentFactory().createEnrollment();
        verificator = DPFPGlobal.getVerificationFactory().createVerification();
        
        //Add the listener here to the calling class
        try{
            pListener = (ProviderListener) mClass;
        }catch(ClassCastException e){
            System.out.println("Error : " + e.getMessage());
        }
       
    }
    
    public DPFPProvider( boolean forStatus ) {
        
        if(forStatus){
            //this.capturer = DPFPGlobal.getCaptureFactory().createCapture();
        }
        //enroller = DPFPGlobal.getEnrollmentFactory().createEnrollment();
        //verificator = DPFPGlobal.getVerificationFactory().createVerification();
        
        //Add the listener here to the calling class
        
    }
    
    public DPFPCapture getCapturerForStatus(){
        
        if(capturer != null){
            // todo: learn about throw and implement it
        }
        return DPFPGlobal.getCaptureFactory().createCapture();
    }
    
    

    /**
     * Initialize listener for application to watch the status of the device
     */
    public void init(){
        System.out.println("Init DPFPProvider");
        try{
            capturer.addDataListener(new DPFPDataAdapter() {
                @Override public void dataAcquired(final DPFPDataEvent e) {
                    makeReport("The fingerprint sample was captured.");
                    setPrompt("Scan the same fingerprint again.");
                    isFPDataReady = true;
                    Image in = process(e.getSample());
                    pListener.onDataListener(e.getSample() , in);
                }
            });

            capturer.addReaderStatusListener(new DPFPReaderStatusAdapter() {
                @Override 
                public void readerConnected(final DPFPReaderStatusEvent e) {
                    readerStatus = true;
                    isFPConnected = true;
                    System.out.println("finger print connected status : " + isFPConnected);
                    makeReport("The fingerprint reader was connected.");
                    pListener.onReaderStatusListener(true);
                }
                @Override 
                public void readerDisconnected(final DPFPReaderStatusEvent e) {
                    readerStatus = false;
                    isFPConnected = false;
                    makeReport("The fingerprint reader was disconnected.");
                    pListener.onReaderStatusListener(false);
                }
            });

            capturer.addSensorListener(new DPFPSensorAdapter() {
                @Override public void fingerTouched(final DPFPSensorEvent e) {
                    makeReport("The fingerprint reader was touched.");
                    isFPSensorReady =true;
                    pListener.onSensorListener(true);

                }
                @Override public void fingerGone(final DPFPSensorEvent e) {
                    makeReport("The finger was removed from the fingerprint reader.");
                    isFPSensorReady = false;
                    pListener.onSensorListener(false);
                }
            });
            capturer.addImageQualityListener(new DPFPImageQualityAdapter() {
                @Override public void onImageQuality(final DPFPImageQualityEvent e) {
                    if (e.getFeedback().equals(DPFPCaptureFeedback.CAPTURE_FEEDBACK_GOOD)) {
                        makeReport("The quality of the fingerprint sample is good.");
                        isFPImageQualityGood = true;
                        pListener.onImageQualityListener(true);
                    }
                    else {
                        makeReport("The quality of the fingerprint sample is poor.");
                        isFPImageQualityGood = false;
                        pListener.onImageQualityListener(false);
                    }
                }
            });
        }catch(Exception e){
            System.out.println("Error : " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    /**
     * method to get the satus of the fingerprint if connected
     */
    public boolean getStatus(){
      return this.readerStatus;
    }

    /**
     * call in other to process sample in the lister
     * @param sample of the finger print taken from the dataLister
     * @return 
     */
    public Image process(DPFPSample sample)
    {
        // Draw fingerprint sample image.
        return convertSampleToImage(sample);
    }
    
    /**
     * Used to extract features out of a fingerprint sample for the purpose of enrollment and verification
     * @param sample sample data to extract
     * @param purpose purpose of extraction enrollemnt / verification
     * @return extract 
     */
    public DPFPFeatureSet extractFeature(DPFPSample sample, DPFPDataPurpose purpose){
        DPFPFeatureExtraction extractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
        DPFPFeatureSet features = null;
        try{
           features = extractor.createFeatureSet(sample, purpose);
        }catch (DPFPImageQualityException ex) {
            System.out.println("extraction error : " + ex.getMessage());
            
        }
        
        return features;
    }
    
    /**
     * to enroll finger for the first time
     * @param sample sample taken from the dataLister
     * @return template of the enrollemnt 
     * @throws DPFPImageQualityException 
     */
    public DPFPTemplate enrollFinger(DPFPSample sample) {
        DPFPFeatureSet features = extractFeature(sample, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);
        if(features != null){
            
            try {
                enroller.addFeatures(features);
            } catch (DPFPImageQualityException ex) {
               enroller.clear();
               Registration.count = -1;//sets the count back to zero
                AdminRegistration.count = -1;//please in upgrade lets implement a listener for this
            }

            switch(enroller.getTemplateStatus()){
                case TEMPLATE_STATUS_READY:
                    template = enroller.getTemplate();
                    enroller.clear();
                    System.out.println("FIngerprint enrolled");
                    return template;
                case TEMPLATE_STATUS_FAILED:
                    System.out.println("FIngerprint enrolled fail");
                    enroller.clear();
                    break;
                case TEMPLATE_STATUS_INSUFFICIENT:
                    System.out.println("template insufficient : " + enroller.getFeaturesNeeded());
                    
                    break;
                case TEMPLATE_STATUS_UNKNOWN:
                    System.out.println("template unknown : " + enroller.getTemplateStatus());
                    break;
            }
        }
        return null;
    }
    
    /**
     * Used to verify finger against a candidate 
     * @param temp template of the enrolled finger
     * @param sample sample of the new finger to probe
     * @return true of false if verifies or not
     */
    public boolean verifyFinger(DPFPTemplate temp, DPFPSample sample){
        DPFPFeatureSet features = extractFeature(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);
        System.out.println("verify fingerprint");
        
        if(temp != null){
            DPFPVerificationResult result = verificator.verify(features, temp);
            System.out.println("FAR Requested " + verificator.getFARRequested());
            System.out.println("verify:FAR - " + result.getFalseAcceptRate());
            if(result.isVerified()){
                return true;
            }else{
                return false;
            }
        }else{
            System.out.println("template is null");
        }
        
        return false;
    }
    
    public DPFPTemplate setOldTemplateFromStream(InputStream stream){
        DPFPTemplate temp = DPFPGlobal.getTemplateFactory().createTemplate();
        byte[] data;
        try {
            data = new byte[stream.available()];
            stream.read(data);
            stream.close();
            temp.deserialize(data);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        
        return temp;
    }
    
    public InputStream createStreamFromTemplate(Image image){
        System.out.println("creating stream from template");
        InputStream in, stream = null;
//        in = new ByteArrayInputStream(t.serialize());
        BufferedImage bfImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        try {
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bfImage, "tif", baos);
            
            stream = new ByteArrayInputStream(baos.toByteArray());
            
        } catch (IOException ex) {
            System.out.println("exrror creating stream form template : " + ex.getMessage());
        }
         return stream;
        
    }
    
    /**
     * Used to convert a byte to a template for the purpose of verification
     * @param bytes of the former enrolled template
     * @return 
     */
    public DPFPTemplate createTemplateFromByte(byte[] bytes){
        template = DPFPGlobal.getTemplateFactory().createTemplate();
        template.deserialize(bytes);
        
        return template;
    }
    
    
    /**
     * Used to convert a template to a string to store in the database
     * @param temp templat to convert
     * @return 
     */
    public String createStringFromTemplate(DPFPTemplate temp){
        byte[] bytes = temp.serialize();
        return new BASE64Encoder().encode(bytes);
    }
    
    public byte[] createByteFromStringTemplate(String tempString){
        byte[] candidateByte = null;
            try {
                candidateByte = new BASE64Decoder().decodeBuffer(tempString);
                
                template = DPFPGlobal.getTemplateFactory().createTemplate();
                template.deserialize(candidateByte);


            } catch (IOException ex) {
                System.out.println("Io exception decoding string : " + ex.getMessage() );
            }
            
        return candidateByte;
    }
    
    /**
     * Called to display the fingerprint image on a label as an icon
     * @param labelCon the JLabel to set the fingerprint image on
     * @param imageData the Tmage data to display on screen
     */
    public void showFingerPrint(JLabel labelCon, Image imageData ){
        labelCon.setIcon(new ImageIcon(imageData.getScaledInstance(labelCon.getWidth(), labelCon.getHeight()+5, Image.SCALE_AREA_AVERAGING)));
    }
    

    /**
     * called to start capturing the finger print on the device
     */
    public void start()
    {
//        if(isFPConnected){
            if(!capturer.isStarted()){
                try{
                    System.out.println("connected : " + isFPConnected);
                    System.out.println("sensor : " + isFPSensorReady);
                    System.out.println("Image : " + isFPImageQualityGood);
                    System.out.println("data : " + isFPDataReady);
                     capturer.startCapture();
                     setPrompt("Using the fingerprint reader, scan your fingerprint.");
                }catch(Exception e){
                    System.out.println("Unable to start capturer.. : " + e.getMessage());
                    e.printStackTrace();
                }
            }else{
                stop();
                start();
            }
//        }else{
//            System.out.println("no finger print device connected");
//        }
    }

    /**
     * call this when you are not ready to take any fingerprint again from the device
     */
    public void stop()
    {
        capturer.stopCapture();
        setPrompt("Cpaturer stop...");
    }

    /**
     * used to set the status of the fingerpsrint
     * @param string  string message to log
     */
    public void setStatus(String string) {
//        status.setText(string);
        System.out.println("Finger Status : " + string);
    }
    public void setPrompt(String string) {
//        prompt.setText(string);
        System.out.println("Finger Prompt : " + string );
    }

    public void makeReport(String string) {
//        log.append(string + "\n");
        System.out.println("Finger : " + string);
    }


    /**
     * used to convert a DPFPSample image to Image 
     * @param sample
     * @return 
     */
    protected Image convertSampleToImage(DPFPSample sample) {

        return DPFPGlobal.getSampleConversionFactory().createImage(sample);
    }


    /**
     * class using this class most implement this listener
     */
    public interface ProviderListener{
        
        public void onDataListener(DPFPSample sample, Image imageData);
        public void onReaderStatusListener(Boolean isConnected);
        public void onSensorListener(Boolean isConnected);
        public void onImageQualityListener(Boolean isCaptureGood);

    }

}
