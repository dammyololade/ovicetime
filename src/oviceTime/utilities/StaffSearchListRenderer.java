/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.utilities;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import oviceTime.model.StaffModel;

/**
 *
 * @author dammyololade
 */
public class StaffSearchListRenderer extends JPanel implements ListCellRenderer<StaffModel>{
    
    private final JLabel lbfullname = new JLabel();
    private final JLabel lbmatric = new JLabel();
    private final JLabel lbdepartment = new JLabel();
    private final JLabel lblevel = new JLabel();
    
    public StaffSearchListRenderer(){
        init();
    }
    
    private void init(){
        
        setLayout(new BorderLayout(5, 5));
        
        JPanel panelCenter = new JPanel(new BorderLayout());//this, BoxLayout.PAGE_AXIS));
        panelCenter.setOpaque(false);
        
        JPanel panelTop = new JPanel(new FlowLayout(FlowLayout.LEFT));//new GridLayout(2, 2));
        JPanel panelBottom = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelTop.setOpaque(false);
        panelBottom.setOpaque(false);
        
//        panelTop.add(lbfullname);
//        panelTop.add(lbmatric);
//        
//        panelBottom.add(lbdepartment);
//        panelBottom.add(lblevel);
//        
//        panelCenter.add(panelTop, BorderLayout.NORTH);
        panelCenter.add(lbfullname, BorderLayout.CENTER);
        
        
//        this.add(lblevel, BorderLayout.EAST);
        this.add(panelCenter, BorderLayout.CENTER);
    }
    private Border getPanelborder(){
        
        Border padding =  BorderFactory.createEmptyBorder(4, 4, 4, 4);
        Border matte = BorderFactory.createMatteBorder(0, 3, 0, 0, new Color(0,191,233));//a
        return BorderFactory.createCompoundBorder(matte, padding);
    }
    
    private Border getEmptyBorder(){
         return BorderFactory.createEmptyBorder(4, 4, 4, 4);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends StaffModel> list, StaffModel staff, int index, boolean isSelected, boolean cellHasFocus) {
         setBorder(getEmptyBorder());//Set the default border here
 
            lbfullname.setText(staff.getStaffFullname());
            lbfullname.setFont(new Font(getFont().getName(), Font.PLAIN, 14));
            //lbmatric.setFont(new Font(getFont().getName(), Font.BOLD, 10)); 

            lbfullname.setForeground(Color.white);
            lbfullname.setBorder(new EmptyBorder(10, 10, 10, 10));

    //        
            lbfullname.setBackground(list.getSelectionBackground());
           


            if(isSelected){

               // lbfullname.setForeground(list.getSelectionForeground());
               lbfullname.setFont(new Font(getFont().getFontName(), Font.BOLD, 12));
                setBackground(list.getSelectionBackground());
                setBorder(getPanelborder());

            }else {

                lbfullname.setForeground(Color.black);

                setBackground(list.getBackground());
                //setBorder(getEmptyBorder());
                lbfullname.setBorder(new EmptyBorder(10, 10, 10, 10));
            }

              return this;   
    }

}
