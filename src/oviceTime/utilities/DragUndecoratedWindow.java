/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.utilities;

import javax.swing.JFrame;

/**
 *
 * @author user
 */
public class DragUndecoratedWindow { 
  
     
    private int x1;
    private int y1;
    private int x2;
    private int y2;
     
    private int positionx;
    private int positiony;
     
    private JFrame frame;
    
    public DragUndecoratedWindow(JFrame frame) {
        this.frame = frame;
    }
         
        public void moveWindow(java.awt.event.MouseEvent evt){
             
        this.positionx = evt.getXOnScreen();
        this.positiony = evt.getYOnScreen();
         
        if(this.positionx > this.x1){
            this.x2 = this.positionx - this.x1;
            this.frame.setLocation(this.frame.getX() + this.x2, this.frame.getY());
        }else if(this.positionx < this.x1){
            this.x2 =  this.x1 - this.positionx;
            this.frame.setLocation(this.frame.getX() - this.x2, this.frame.getY());
        }
        if(this.positiony > this.y1){
            this.y2 = this.positiony - this.y1;
            this.frame.setLocation(this.frame.getX(), this.frame.getY() + this.y2);
        }else if(this.positiony < this.y1){
            this.y2 =  this.y1 - this.positiony;
            this.frame.setLocation(this.frame.getX(), this.frame.getY() - this.y2);
        }
        this.x1 = this.positionx;
        this.y1 = this.positiony;
        }
     
     
    public void onPress(java.awt.event.MouseEvent evt){
        this.x1 = evt.getXOnScreen();
        this.y1 = evt.getYOnScreen();
    }

    
}
