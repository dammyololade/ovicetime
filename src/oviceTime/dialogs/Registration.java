/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.dialogs;

import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;

import config.AppConfig;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import java.io.InputStream;

import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafxbeta.StaffRegistrationController;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import oviceTime.MainHomeForm;
import oviceTime.database.DbHelper;
import oviceTime.model.StaffModel;
import oviceTime.listeners.OnFormClosedListener;
import oviceTime.listeners.OnStaffRegisteredListener;
import oviceTime.listeners.OnUpdateListener;
import oviceTime.utilities.DPFPProvider;


/**
 *
 * @author WeaverBird
 */
public class Registration extends javax.swing.JDialog implements DPFPProvider.ProviderListener, OnStaffRegisteredListener,
                OnFormClosedListener{
    
    Frame windowFrame;
    
    private static DPFPProvider dProvider;
    
    private String fingerString;//
    
    private OnUpdateListener listener;
    private StaffModel model;
   
    private List<StaffModel> stafflist;

    
    InputStream stream;
    DbHelper dbHelper;
    //private OnUpdateListener listener;
    
    public static int count = 0;//this was made static because when imagequality exception
                                //is catched in DFProvider class, it sets the count back to zero   
    private JFrame window;
   

    /**
     * Creates new form StudentRegFormPanel
     */
    public Registration(java.awt.Frame parent, boolean modal){
        
        super(parent, modal);
        windowFrame = parent;
        initComponents();        
        this.setUndecorated(true);     
       
        dProvider = new DPFPProvider(this);
        init();
        //initialize capturer to be use with fingerprint reader to register listener on the device
        this.initCapturer();
        
        dbHelper = new DbHelper();
        dbHelper.connect();     
       
        submitButton.setEnabled(false);
        updateInfo(false, "Enroll your Finger on the Scanner 4 times");       
        
    }    
    
    private void initCapturer(){
        this.addComponentListener(new ComponentAdapter(){
            @Override public void componentShown(ComponentEvent e) {
                dProvider.init();
                dProvider.start();
                
            }
            @Override public void componentHidden(ComponentEvent e) {
                dProvider.stop();
            }
            
        });
        
    }   
    
     
    /**
     * this method is transfered from MyCourseform to set the OnUpdate listener for the class
     * @param listener 
     */  
    public void setOnUpdateStaffListener(OnUpdateListener listener){
        this.listener = listener;
              
    }
    
     public void updateInfo(boolean success, String text){
        
        //alertLabel.setBackground(Color.RED);
        if(success){
            alertLabel.setBackground(Color.GREEN);
            alertLabel.setForeground(Color.BLACK);
        }
        else{
             alertLabel.setBackground(new Color(153, 0 , 0));
             alertLabel.setForeground(Color.WHITE);
        }        
        alertLabel.setText(text);
     }   
    
    
//    public void setStudentModel(DefaultListModel<StudentData> model){
//        this.studentModel = model;
//    }
    /**
     * this creates a listener for studentRegisstration panel when a new student is added
     * @param listener 
     */
//    public void setOnUpdateCourseListener(OnUpdateListener listener){
//        this.listener = listener;
//              
//    }

    private void init(){
        
        setLocationRelativeTo(null);
//        setSize(400, 550);
        dProvider = new DPFPProvider(this);
        
       
    }
    
    private void finalise(){
        dProvider.stop();
        try{
            MainHomeForm.dProvider.start();
        }catch(NullPointerException e){
            System.err.println("MainHome error : " + e.getMessage());
        }
        this.setVisible(false);      
        try {
            this.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(Registration.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    
   
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        studentFormPanel = new javax.swing.JPanel();
        registrationPanel = new javax.swing.JPanel();
        fingerprintImage = new javax.swing.JLabel();
        submitButton = new javax.swing.JButton();
        registerAlertLabel = new javax.swing.JLabel();
        alertLabel = new javax.swing.JLabel();
        CloseButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Student Registrationj");
        setMaximumSize(new java.awt.Dimension(350, 450));
        setMinimumSize(new java.awt.Dimension(350, 450));
        setModal(true);
        setResizable(false);
        setSize(new java.awt.Dimension(350, 400));

        studentFormPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 51), 1, true));
        studentFormPanel.setPreferredSize(new java.awt.Dimension(350, 450));
        studentFormPanel.setRequestFocusEnabled(false);

        registrationPanel.setPreferredSize(new java.awt.Dimension(350, 385));

        fingerprintImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        fingerprintImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/fingerprint-dark-2.png"))); // NOI18N
        fingerprintImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        fingerprintImage.setPreferredSize(new java.awt.Dimension(115, 115));

        submitButton.setText("Continue");
        submitButton.setMaximumSize(new java.awt.Dimension(100, 28));
        submitButton.setMinimumSize(new java.awt.Dimension(100, 28));
        submitButton.setPreferredSize(new java.awt.Dimension(100, 28));
        submitButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                submitButtonbutton(evt);
            }
        });
        submitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout registrationPanelLayout = new javax.swing.GroupLayout(registrationPanel);
        registrationPanel.setLayout(registrationPanelLayout);
        registrationPanelLayout.setHorizontalGroup(
            registrationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registrationPanelLayout.createSequentialGroup()
                .addGroup(registrationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, registrationPanelLayout.createSequentialGroup()
                        .addGap(192, 192, 192)
                        .addComponent(submitButton, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(registrationPanelLayout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(fingerprintImage, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        registrationPanelLayout.setVerticalGroup(
            registrationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(registrationPanelLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(fingerprintImage, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 93, Short.MAX_VALUE)
                .addComponent(submitButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        registerAlertLabel.setBackground(new java.awt.Color(33, 63, 87));
        registerAlertLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        registerAlertLabel.setForeground(new java.awt.Color(255, 255, 255));
        registerAlertLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/profile.png"))); // NOI18N
        registerAlertLabel.setText("Create Profile");
        registerAlertLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 13, 0, 10));
        registerAlertLabel.setIconTextGap(10);
        registerAlertLabel.setOpaque(true);
        registerAlertLabel.setPreferredSize(new java.awt.Dimension(316, 40));

        alertLabel.setBackground(new java.awt.Color(153, 0, 0));
        alertLabel.setForeground(new java.awt.Color(255, 255, 255));
        alertLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        alertLabel.setText("Enroll your fingerprint on the Scanner Four times");
        alertLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 10, 1, 1));
        alertLabel.setOpaque(true);
        alertLabel.setPreferredSize(new java.awt.Dimension(350, 25));

        CloseButton.setBackground(new java.awt.Color(33, 63, 87));
        CloseButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/window-close-18.png"))); // NOI18N
        CloseButton.setToolTipText("");
        CloseButton.setBorder(null);
        CloseButton.setContentAreaFilled(false);
        CloseButton.setOpaque(true);
        CloseButton.setPreferredSize(new java.awt.Dimension(50, 40));
        CloseButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                CloseButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                CloseButtonMouseExited(evt);
            }
        });
        CloseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CloseButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout studentFormPanelLayout = new javax.swing.GroupLayout(studentFormPanel);
        studentFormPanel.setLayout(studentFormPanelLayout);
        studentFormPanelLayout.setHorizontalGroup(
            studentFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(studentFormPanelLayout.createSequentialGroup()
                .addComponent(registrationPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(studentFormPanelLayout.createSequentialGroup()
                .addGroup(studentFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(studentFormPanelLayout.createSequentialGroup()
                        .addComponent(registerAlertLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(CloseButton, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(alertLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        studentFormPanelLayout.setVerticalGroup(
            studentFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, studentFormPanelLayout.createSequentialGroup()
                .addGroup(studentFormPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(registerAlertLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CloseButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0)
                .addComponent(alertLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(registrationPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(studentFormPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(studentFormPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

    int real_count = 0 ;
     public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Registration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Registration dialog = new Registration(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        
                        System.exit(0);
                    }
                });
                dialog.setSize(350, 450);
                dialog.setVisible(true);
            }
        });
     }
     
    private void submitButtonbutton(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_submitButtonbutton
        // TODO add your handling code here:
        
    }//GEN-LAST:event_submitButtonbutton

    private void submitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitButtonActionPerformed
        // TODO add your handling code here
       window = new JFrame();
       JFXPanel jfxPanel = new JFXPanel();
       
            Platform.runLater(() -> {

            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/javafxbeta/StaffRegistration.fxml"));
                Parent root = loader.load();
                
                Scene scene = new Scene(root, 800, 600);
                scene.getStylesheets().add(getClass().getResource("/css/test.css").toExternalForm());
                jfxPanel.setScene(scene);                

                SwingUtilities.invokeLater(() -> {
                    window.add(jfxPanel);                   
                    window.setUndecorated(true);                   
                    window.pack(); 
                     window.setLocationRelativeTo(null);
                    window.setVisible(true);
                    StaffRegistrationController.setOnStaffRegisteredlistener(this);
                    StaffRegistrationController.setOnFormClosedListener(this);
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        });      
        finalise();
        
       
    }//GEN-LAST:event_submitButtonActionPerformed

    private void CloseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CloseButtonActionPerformed
        // TODO add your handling code here:
        finalise();
    }//GEN-LAST:event_CloseButtonActionPerformed

    private void CloseButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CloseButtonMouseExited
        // TODO add your handling code here:
        CloseButton.setBackground(Color.BLACK);
        
    }//GEN-LAST:event_CloseButtonMouseExited

    private void CloseButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CloseButtonMouseEntered
        // TODO add your handling code here:
        CloseButton.setBackground(new Color(153,0,51));
    }//GEN-LAST:event_CloseButtonMouseEntered


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CloseButton;
    private javax.swing.JLabel alertLabel;
    private javax.swing.JLabel fingerprintImage;
    private javax.swing.JLabel registerAlertLabel;
    private javax.swing.JPanel registrationPanel;
    private javax.swing.JPanel studentFormPanel;
    private javax.swing.JButton submitButton;
    // End of variables declaration//GEN-END:variables

    //boolean verifyFingerExist = false;//note this is used to set a flag if a student has regitered before or not
    @Override
    public void onDataListener(DPFPSample sample, Image imageData) {
            //dProvider.stop();
       // try{
//            Enroll finger return template if finger has been enrolled
            DPFPTemplate template = dProvider.enrollFinger(sample);
            dProvider.showFingerPrint(fingerprintImage, imageData);//Shows fingerprint on label
            
            
            if(template != null){
                //get string value of template to save in Database
                        //DPFPTemplate ntemplate = dProvider.enrollFinger(sample);
                         MainHomeForm.fingerstring = dProvider.createStringFromTemplate(template);
                         //Stop the fingerprint reader from reading finger again
                         dProvider.stop();
                         count = 0;
                         real_count = 0;
                         submitButton.setEnabled(true);                       
                        
                         updateInfo(true, "Enrollment completed, Proceed with the Registration");
                
            }     
            else {
                
                 real_count = real_count + 1;
                 count = count + 1;
                updateInfo(false, "Enroll your finger " + (4 - count) +"time(s)");
                if(real_count > count ){//check if count is returned as zero when the fingerprint image quality is low
                    updateInfo(false, "Please Kindly Ensure Use Only One Finger for time(s)");
                    real_count = count;//resets the value of real count when the person finger image is low or not enrolled
                }                
                
            }
            
            

        
    }

    @Override
    public void onReaderStatusListener(Boolean isConnected) {
        
        if(isConnected){
           updateInfo(false, "Enroll One of your fingers on the fingerprint 4 times");
           
       }
        else{
            updateInfo(true, "Your fingerprint is disconnected");
        }             
        
    }

    @Override
    public void onSensorListener(Boolean isConnected) {
        //updateInfo(false, "Sensor is currently : " + isConnected);
        
    }

    @Override
    public void onImageQualityListener(Boolean isCaptureGood) {
        System.out.println("the image is not good nigga, i can gte here " + isCaptureGood);
        updateInfo(true, "Image Quality is : " + isCaptureGood);
        
    }

    @Override
    public void OnStaffREgistered(String fullname) {
        window.setVisible(false);
        listener.onUpdateStaff(fullname);//Notifies the app main to switch cards
    }

    @Override
    public void onFormClosed() {
       window.setVisible(false);
    }
}
