/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.dialogs;

import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.util.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import oviceTime.MainHomeForm;
import oviceTime.database.DbHelper;
import oviceTime.model.AttendanceModel;
import oviceTime.model.FingerModel;
import oviceTime.listeners.OnUpdateListener;
import oviceTime.utilities.DPFPProvider;
import oviceTime.listeners.OnAttendanceMarkedListener;

/**
 *
 * @author user
 */
public class AttendanceDialog extends javax.swing.JDialog implements DPFPProvider.ProviderListener{
    
     public static DPFPProvider dProvider;
    public DPFPEnrollment enroller = DPFPGlobal.getEnrollmentFactory().createEnrollment();
    public DPFPVerification verificator = DPFPGlobal.getVerificationFactory().createVerification();
    private DPFPTemplate template1, template2;
    
    private static OnUpdateListener listener;
    private OnAttendanceMarkedListener sListener;
    private final DbHelper db;
    private int count;
    static Frame windowFrame;
    
//    private Thread updateCpatureThread;
//    private Thread nextThread;
//    private final Runnable updateRunnable;
//    private final Runnable nextCounterRunnable;
    
    public InputStream stream;
    
    private final int NEXT_TIME = 10;
    private boolean NEXT_ACTIVE = true;
    
    private List<FingerModel> fingerList = null;
    private String currentTime;
    private Date currentDate;
    public static boolean scannerStatus;
    private boolean isAttendanceMarked = false;
    
    

    /**
     * Creates new form AttendanceDialog
     */
    public AttendanceDialog(java.awt.Frame parent, boolean modal) {
        
        super(parent, modal);
        this.setUndecorated(true);
         initComponents();
        
        windowFrame = parent;
        
        dProvider = new DPFPProvider(this);//Initialize the Digital Provider 
        init();
        initCapturer();
      
        attendanceStatusLabel.setVisible(false);
        db = new DbHelper(true); // connect to database.        
    }
    
    private void init(){
        setLocationRelativeTo(null);
        dProvider = new DPFPProvider(this);     
       ButtonGroup group = new ButtonGroup();
       group.add(signInButton);
       group.add(signOutButton);      
       
    }
    
/**
 * 
 * @return current time
 */
    private void setTime(){
        Calendar calendar = Calendar.getInstance();
        String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String min = String.valueOf(calendar.get(Calendar.MINUTE));
        String sec = String.valueOf(calendar.get(Calendar.SECOND));

        String appendtime = hour +":"+min+":"+sec;
        currentTime =  appendtime;
    }
    
    
   
    
    /**
     * Initialize the capturer when this dialog is building it components
     * this 
     */
    public void initCapturer(){ 
        
        System.out.println("init capturer in add component listener in attendance dialog");      
        //When a component is added to this dialog 
        this.addComponentListener(new ComponentAdapter(){            
            @Override public void componentShown(ComponentEvent e) {
                System.out.println("init capture in add component is shown i.e component are shown in dialog");
                dProvider.init();
                dProvider.start();
            }
            @Override public void componentHidden(ComponentEvent e) {
                System.out.println("inicapture in add component is hidden i.e component are hiiden in attendance dialog");
                dProvider.stop();
            }            
        });        
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("Done dialog done");
                dProvider.stop();
                db.close();
                MainHomeForm.dProvider.start();  
            };            
        });        
    }    
    
    public boolean fingerPrintAvailable(){
        return dProvider.isFPConnected ;
    }
    
    public void setOnUpdatelistener(OnUpdateListener listener){
        this.listener = listener;
    }
    
    /**
     * sets the listener to notify when attendance has been Marked
     * @param listener 
     */
    public void setOnAttendanceMarkedlistener(OnAttendanceMarkedListener listener){
        sListener = listener;
    }
    
     public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    
    
    public void exit(){
        dProvider.stop();
        MainHomeForm.dProvider.start();
        if(isAttendanceMarked){//notify the listener if attendance has been marked
            sListener.OnAttendanceMarked();
        }
        
        this.setVisible(false);
        try {
            System.out.println("Finalizing attendance dialog ...");
            this.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(AttendanceDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void updateStatus()
    {
        // Show number of samples needed.
        dProvider.setStatus(String.format("Fingerprint samples needed: %1$s", enroller.getFeaturesNeeded()));
    }
  
    
    public void updateInfo(boolean success, String text){
        
        if(success){
            infoCon.setBackground(Color.GREEN);
            infoLabel.setForeground(Color.BLACK);
        }
        else{
            infoCon.setBackground(new Color(153,0,0));
            infoLabel.setForeground(Color.WHITE);
        }
        
        
        infoLabel.setText(text);
    }
    
   
    
    public  void updateAttendanceStatus(boolean  success, String text){
        if(success){
            attendanceStatusLabel.setForeground(Color.GREEN);
        }else{
            attendanceStatusLabel.setForeground(Color.RED);
        }
        attendanceStatusLabel.setText(text);
    }
    
    private void markAttendance(FingerModel model) { 
        
       
        
        setTime();
        LocalDate localDate = LocalDate.now();
        String newDate = DateTimeFormatter.ofPattern("yyy-MM-dd").format(localDate);
        AttendanceModel attendance;
        int result;
        if(signInButton.isSelected()){
            attendance = new AttendanceModel(0, model.getStaffId(), newDate, null, null);
            result = db.markAttendance(attendance, true);
        }else{
            attendance = new AttendanceModel(0, model.getStaffId(), newDate, null, currentTime);
            result = db.markAttendance(attendance, false);
        }
       
       
       if(result > 0){
           AlertDialog dialog = new AlertDialog(windowFrame, true);
           dialog.setMessage("Welcome: " + model.getfullname());
           dialog.setVisible(true); 
           isAttendanceMarked = true;
       }
       else{
           
       }
       
        dProvider.start();
        updateInfo(false, " Place Enrolled Hand on the finger print Scanner");
        ImageIcon imageIcon = new ImageIcon(new ImageIcon(getClass().getResource("/oviceTime//icon/fingerprint-dark-2.png")).getImage());
        fingerprintImage.setIcon(imageIcon);
    }
    
   
   /*
    */
    public void setFingerList(List<FingerModel> fingerList){
        this.fingerList = fingerList;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        attendaceTitle = new javax.swing.JLabel();
        CloseButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        infoCon = new javax.swing.JPanel();
        infoLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel4 = new javax.swing.JPanel();
        fingerprintImage = new javax.swing.JLabel();
        comboxPanel = new javax.swing.JPanel();
        signInButton = new javax.swing.JRadioButton();
        signOutButton = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        attendanceStatusLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(350, 450));

        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(33, 63, 87), 1, true));
        jPanel1.setPreferredSize(new java.awt.Dimension(350, 450));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 2, 0, 2, new java.awt.Color(60, 63, 65)));
        jPanel2.setName(""); // NOI18N
        jPanel2.setPreferredSize(new java.awt.Dimension(350, 40));

        attendaceTitle.setBackground(new java.awt.Color(33, 63, 87));
        attendaceTitle.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        attendaceTitle.setForeground(new java.awt.Color(255, 255, 255));
        attendaceTitle.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        attendaceTitle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/fingerprint.png"))); // NOI18N
        attendaceTitle.setText("Biometrick Attendance");
        attendaceTitle.setToolTipText("");
        attendaceTitle.setAlignmentX(0.5F);
        attendaceTitle.setIconTextGap(30);
        attendaceTitle.setMaximumSize(new java.awt.Dimension(350, 50));
        attendaceTitle.setMinimumSize(new java.awt.Dimension(450, 50));
        attendaceTitle.setOpaque(true);
        attendaceTitle.setPreferredSize(new java.awt.Dimension(310, 40));

        CloseButton.setBackground(new java.awt.Color(33, 63, 87));
        CloseButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/window-close-18.png"))); // NOI18N
        CloseButton.setBorder(null);
        CloseButton.setContentAreaFilled(false);
        CloseButton.setMinimumSize(new java.awt.Dimension(30, 40));
        CloseButton.setOpaque(true);
        CloseButton.setPreferredSize(new java.awt.Dimension(40, 40));
        CloseButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                CloseButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                CloseButtonMouseExited(evt);
            }
        });
        CloseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CloseButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(attendaceTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(CloseButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(attendaceTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(CloseButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel3.setForeground(new java.awt.Color(125, 119, 119));
        jPanel3.setMaximumSize(new java.awt.Dimension(400, 500));
        jPanel3.setMinimumSize(new java.awt.Dimension(400, 500));
        jPanel3.setName(""); // NOI18N
        jPanel3.setPreferredSize(new java.awt.Dimension(350, 410));

        infoCon.setBackground(new java.awt.Color(153, 0, 0));
        infoCon.setToolTipText("");
        infoCon.setMaximumSize(new java.awt.Dimension(450, 30));
        infoCon.setMinimumSize(new java.awt.Dimension(450, 20));
        infoCon.setPreferredSize(new java.awt.Dimension(350, 30));
        infoCon.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 10));

        infoLabel.setForeground(new java.awt.Color(255, 255, 255));
        infoLabel.setText(" Place Enrolled Hand on the finger print Scanner");
        infoCon.add(infoLabel);
        infoCon.add(jSeparator1);

        jPanel4.setMaximumSize(new java.awt.Dimension(450, 250));
        jPanel4.setMinimumSize(new java.awt.Dimension(450, 250));
        jPanel4.setOpaque(false);
        jPanel4.setPreferredSize(new java.awt.Dimension(350, 250));
        jPanel4.setVerifyInputWhenFocusTarget(false);

        fingerprintImage.setBackground(new java.awt.Color(255, 255, 255));
        fingerprintImage.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        fingerprintImage.setForeground(new java.awt.Color(215, 212, 212));
        fingerprintImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        fingerprintImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/fingerprint-dark-2.png"))); // NOI18N
        fingerprintImage.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(208, 205, 205), 1, true));
        fingerprintImage.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        fingerprintImage.setIconTextGap(0);
        fingerprintImage.setMaximumSize(new java.awt.Dimension(150, 200));
        fingerprintImage.setMinimumSize(new java.awt.Dimension(150, 200));
        fingerprintImage.setPreferredSize(new java.awt.Dimension(150, 200));
        fingerprintImage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        comboxPanel.setPreferredSize(new java.awt.Dimension(100, 30));

        signInButton.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        signInButton.setForeground(new java.awt.Color(33, 63, 87));
        signInButton.setSelected(true);
        signInButton.setText("Sign-in");
        signInButton.setIconTextGap(10);
        signInButton.setName(""); // NOI18N
        signInButton.setPreferredSize(new java.awt.Dimension(100, 30));
        signInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signInButtonActionPerformed(evt);
            }
        });

        signOutButton.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        signOutButton.setForeground(new java.awt.Color(33, 63, 87));
        signOutButton.setText("Sign-out");
        signOutButton.setContentAreaFilled(false);
        signOutButton.setIconTextGap(10);
        signOutButton.setName(""); // NOI18N
        signOutButton.setPreferredSize(new java.awt.Dimension(100, 30));
        signOutButton.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/check-circle.png"))); // NOI18N
        signOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                signOutButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout comboxPanelLayout = new javax.swing.GroupLayout(comboxPanel);
        comboxPanel.setLayout(comboxPanelLayout);
        comboxPanelLayout.setHorizontalGroup(
            comboxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(comboxPanelLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(signInButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(signOutButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(158, Short.MAX_VALUE))
        );
        comboxPanelLayout.setVerticalGroup(
            comboxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(comboxPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(signInButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(signOutButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(comboxPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 399, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addComponent(fingerprintImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(comboxPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fingerprintImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );

        jPanel5.setMaximumSize(new java.awt.Dimension(411, 30));
        jPanel5.setMinimumSize(new java.awt.Dimension(411, 20));
        jPanel5.setPreferredSize(new java.awt.Dimension(400, 50));

        attendanceStatusLabel.setBackground(new java.awt.Color(255, 255, 255));
        attendanceStatusLabel.setFont(new java.awt.Font("Trebuchet MS", 0, 14)); // NOI18N
        attendanceStatusLabel.setForeground(new java.awt.Color(93, 86, 86));
        attendanceStatusLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        attendanceStatusLabel.setText("Success Attendance Taken");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(attendanceStatusLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(attendanceStatusLabel)
                .addGap(12, 12, 12))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(infoCon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(infoCon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(164, 164, 164))
        );

        jPanel1.add(jPanel3, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CloseButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CloseButtonMouseEntered
        // TODO add your handling code here:
        CloseButton.setBackground(new Color(153,0,51));
    }//GEN-LAST:event_CloseButtonMouseEntered

    private void CloseButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CloseButtonMouseExited
        // TODO add your handling code here:
        CloseButton.setBackground(Color.BLACK);
    }//GEN-LAST:event_CloseButtonMouseExited

    private void CloseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CloseButtonActionPerformed
        // TODO add your handling code here:
        exit();
    }//GEN-LAST:event_CloseButtonActionPerformed

    private void signOutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signOutButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_signOutButtonActionPerformed

    private void signInButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_signInButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_signInButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AttendanceDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AttendanceDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AttendanceDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AttendanceDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                AttendanceDialog dialog = new AttendanceDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CloseButton;
    private javax.swing.JLabel attendaceTitle;
    private javax.swing.JLabel attendanceStatusLabel;
    private javax.swing.JPanel comboxPanel;
    private javax.swing.JLabel fingerprintImage;
    private javax.swing.JPanel infoCon;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton signInButton;
    private javax.swing.JRadioButton signOutButton;
    // End of variables declaration//GEN-END:variables

    @Override
    public void onDataListener(DPFPSample sample, Image imageData) {
         dProvider.stop();
        //fingerprintImage.setIcon(new ImageIcon(imageData));//.getScaledInstance(fingerprintImage.getWidth(), fingerprintImage.getHeight()+5, Image.SCALE_DEFAULT)));
        dProvider.showFingerPrint(fingerprintImage, imageData);
        boolean verified = false;
        FingerModel verifiedFInger = null;
        try{
                if(!fingerList.isEmpty()){
                    for(FingerModel model : fingerList){
                        byte[] fingeBytes = dProvider.createByteFromStringTemplate(model.getFinger());
                        DPFPTemplate template = dProvider.createTemplateFromByte(fingeBytes);
                        verified = dProvider.verifyFinger(template, sample);
                         if(verified){                         
                            updateInfo(true, "You are verified: " + model.getfullname());
                            markAttendance(model);
                            break; 
                         }
                    }
                    if(!verified){
                         updateInfo(false, "Fingerprint Not registered");
                          ImageIcon imageIcon = new ImageIcon(new ImageIcon(getClass().getResource("/oviceTime/icon/fingerprint-dark-2.png")).getImage());
                          fingerprintImage.setIcon(imageIcon);
                         dProvider.start();
                    }
                }else{// Finger not available in the database.

                    updateInfo(false , "Cannot Take Attendance Because you have not Registered any FInger");
                    updateAttendanceStatus(false, "You have not Registered "); 

                }   
            
            } catch (NullPointerException ex) {
                System.out.println("Database error : " + ex.getMessage());
            }finally{            
               // updateCapture();
            }        
    }

    @Override
    public void onReaderStatusListener(Boolean isConnected) {
       System.out.println("Reader is currently : " + isConnected);
        if(!isConnected){
            scannerStatus = false;            
        }
    }

    @Override
    public void onSensorListener(Boolean isConnected) {
       
    }

    @Override
    public void onImageQualityListener(Boolean isCaptureGood) {
        
    }
    
    public boolean getStatus(){
       return scannerStatus;
    }

   
}
