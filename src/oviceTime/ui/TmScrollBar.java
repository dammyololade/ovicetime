/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oviceTime.ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicScrollBarUI;

/**
 *
 * @author user
 */
public class TmScrollBar extends BasicScrollBarUI{
    
     Color myTrackColor, myThumbColor, myThumbHoverColor;
    int myScrollBarWidth;
    
    public static ComponentUI createUI(JComponent c){
        return new TmScrollBar();
    }
    
    public TmScrollBar(){
        
//        Load settings = new Load();
//        Color[] colors;
//        colors = settings.
        myTrackColor = new Color(240,240,240);
        myThumbColor = new Color(204,204,204);
        myThumbHoverColor = new Color(79, 195, 247);
        
        myScrollBarWidth = 10;
        
    }
    
    
    private JButton createZeroButton() {
        JButton jbutton = new JButton();
        jbutton.setPreferredSize(new Dimension(0, 0));
        jbutton.setMinimumSize(new Dimension(0, 0));
        jbutton.setMaximumSize(new Dimension(0, 0));
        return jbutton;
    }

    @Override
    protected void setThumbBounds(int x, int y, int width, int height) {
        
        super.setThumbBounds(x, y, 10, height); //To change body of generated methods, choose Tools | Templates.

        // Once there is API to determine the mouse location this will need
        // to be changed.
        //setThumbRollover(false);
    }

    @Override
    public Dimension preferredLayoutSize(Container scrollbarContainer) {
        return super.preferredLayoutSize(scrollbarContainer); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    @Override
    public Dimension getPreferredSize(JComponent c) {
        //return super.getPreferredSize(c); //To change body of generated methods, choose Tools | Templates.
        return (scrollbar.getOrientation() == JScrollBar.VERTICAL)
            ? new Dimension(myScrollBarWidth, 48)
            : new Dimension(48, myScrollBarWidth);
    }
    
    

    
    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
        //super.paintThumb(g, c, thumbBounds); //To change body of generated methods, choose Tools | Templates.

        if(thumbBounds.isEmpty() || !scrollbar.isEnabled())     {
            return;
        }
        
        
        int w = thumbBounds.width;
        int h = thumbBounds.height;
        
        g.translate(thumbBounds.x, thumbBounds.y);
        
        //Graphics gg = g;
        g.setColor(myThumbColor); //new Color(255,153,153));
        g.fillRect(0, 0, w, h);
        
        g.translate(thumbBounds.x, thumbBounds.y);
        
        
    }

    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {
        
        g.setColor(myTrackColor);
        g.fillRect(trackBounds.x, trackBounds.y, trackBounds.width, trackBounds.height);
        //super.paintTrack(g, c, trackBounds); //To change body of generated methods, choose Tools | Templates.
//        Graphics gg = g;
//        gg.setColor(new Color(255,153,153));
//        
//        c.paint(gg);
    }

    @Override
    public boolean isThumbRollover() {
        
        return super.isThumbRollover(); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    @Override
    protected JButton createIncreaseButton(int orientation) {
        return createZeroButton();
    }

    @Override
    protected JButton createDecreaseButton(int orientation) {
        return createZeroButton();
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        super.paint(g, c); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void configureScrollBarColors() {
        super.configureScrollBarColors(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setThumbRollover(boolean active) {
        //super.setThumbRollover(active); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("Over The Tumbs" + active);
    }
}
