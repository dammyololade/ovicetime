/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package oviceTime;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Arrays;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafxbeta.StaffRegistrationController;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import oviceTime.database.DbHelper;
import oviceTime.dialogs.AlertDialog;
import oviceTime.dialogs.ConfirmDialog;
import oviceTime.model.StaffModel;
import oviceTime.ui.TmScrollBar;
import oviceTime.listeners.OnFormClosedListener;
import oviceTime.listeners.OnMailSentListener;
import oviceTime.listeners.OnUpdateListener;

/**
 *
 * @author user
 */
public class StaffListPanel extends javax.swing.JPanel implements OnFormClosedListener, OnUpdateListener, OnMailSentListener{
    
    public DbHelper dbHelper;
    private String selectedName;
    private final String[] columnNames = {"S/N", "Fullname", " Address ", "Phone Number ", " Email "};
    private String [][] data;
    private JTable table;
    final static String STAFF_DETAILS_PANEL = "staffDetailsPanel";
    final static String STAFF_LIST_PANEL = "staffListPanel";
    public static StaffDetailsPanel staffDetailsPanel;
    private JPanel tablePanel;
    private String fullname;
    public JFrame windowFrame;
    private OnUpdateListener listener;
    private JFrame window;
    private String staffEmail;
    
    private StaffModel staffModel;


    /** Creates new form StaffListPanel */
    public StaffListPanel() {
        initComponents();
        dbHelper = new DbHelper(true);
        tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout());
        createStaffTable();
        setUpCardLayout();
        backBtn.setVisible(false);
        editBtn.setVisible(false);
        deleteBtn.setVisible(false);
        mailbtn.setVisible(false);
    }
    
    /**
     * 
     * refresh the table if a new data is in the database 
     */
    public void referesh(){
        createStaffTable();
        setUpCardLayout();
    }
    
     private void setUpCardLayout(){        
       
        this.staffDetailsPanel = new StaffDetailsPanel();      
        
        cardsContainer.add(tablePanel, STAFF_LIST_PANEL);
        cardsContainer.add(staffDetailsPanel, STAFF_DETAILS_PANEL);
       
    }
    
    /**
     * set the window frame from home panel 
     * to instanciate dialogs
     * @param windowFrame 
     */ 
    public void setFrame(JFrame windowFrame){
        this.windowFrame = windowFrame;
    } 
    
    /**
     * load, refresh and switch staff details
     * @param fullname 
     */
    public void loadStaffDetails(String fullname){
            createStaffTable();
            backBtn.setVisible(true);
            editBtn.setVisible(true);
            deleteBtn.setVisible(true);
            mailbtn.setVisible(true);
            StaffListPanel.staffDetailsPanel.refresh(fullname);
            switchCardsInHome(STAFF_DETAILS_PANEL);
    } 
    
    /**
     * set the listener to notify any change with staff data
     * @param listener 
     */
    public void setOnUpdateListener(OnUpdateListener listener){
        this.listener = listener;
    }
    
    /**
     * create and update staff table with lil details
     */
    public void createStaffTable(){
        tablePanel.removeAll();
        
        try{
            data = dbHelper.getAllStaffDetails();
            if(data.length > 0){
                statuslabel.setVisible(false);
                table = new JTable(data, columnNames);
                table.setSelectionMode(1);
                table.setRowHeight(30);      
                //table.setDefaultRenderer(Object.class, new TableRenderer());
                table.setSelectionBackground(new Color(0,191,233));
                table.setSelectionForeground(Color.black);

                table.setBackground(Color.white);
                table.setBorder(new EmptyBorder(8, 8, 8, 8));
                table.setGridColor(Color.white);

                JScrollPane scrollPane = new JScrollPane(table);
                scrollPane.getVerticalScrollBar().setUI(new TmScrollBar());
                scrollPane.setBackground(new Color(240,240,240));
                scrollPane.setBorder(new EmptyBorder(8, 8, 8, 8));
                tablePanel.add(scrollPane, BorderLayout.CENTER);
                
                table.setBorder(null);
                table.getSelectionModel().addListSelectionListener(new TableSelectionListener());
            }
         }catch(NullPointerException dd){
         
         }
        
    }
    
    public void switchCardsInHome(String cardName){
        CardLayout cardLayout = (CardLayout) cardsContainer.getLayout();
        cardLayout.show(cardsContainer, cardName);
    }

    @Override
    public void onFormClosed() {
        window.setVisible(false);
    }

    @Override
    public void onUpdateStaff(String fullname) {
        window.setVisible(false);
        createStaffTable();
        backBtn.setVisible(true);
        editBtn.setVisible(true);
        deleteBtn.setVisible(true);
        mailbtn.setVisible(true);
        StaffListPanel.staffDetailsPanel.refresh(fullname);
    }

    @Override
    public void onStaffDeleted() {
        
    }

    @Override
    public void onMailSent() {
        AlertDialog dialog = new AlertDialog(window, true);        
        dialog.setMessage("Your Mail has been sent");
        dialog.setVisible(true);
        //mailbtn.setEnabled(true);
    }
   
    

    /**
     * this segments listens to the click event on the table
     * selects the date equivalent of the attendance of the student selected
     * used some array tricks
     */
    
    private class TableSelectionListener implements ListSelectionListener{
        
        @Override
        public void valueChanged(ListSelectionEvent e) {
           
            int row = table.getSelectedRow();//gets the index of the selected row
            String selectedRow[] = data[row];
            fullname = selectedRow[1].toString();
           // System.out.println("the selected row is  " + fullname);
            staffEmail = selectedRow[4].toString();
            backBtn.setVisible(true);
            editBtn.setVisible(true);
            deleteBtn.setVisible(true);
            mailbtn.setVisible(true);
            StaffListPanel.staffDetailsPanel.refresh(fullname);
            switchCardsInHome(STAFF_DETAILS_PANEL);
           
        }  
    }
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel6 = new javax.swing.JPanel();
        backBtn = new javax.swing.JButton();
        statuslabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        editBtn = new javax.swing.JButton();
        mailbtn = new javax.swing.JButton();
        deleteBtn = new javax.swing.JButton();
        cardsContainer = new javax.swing.JPanel();

        setPreferredSize(new java.awt.Dimension(800, 550));
        setLayout(new java.awt.BorderLayout());

        jPanel6.setBackground(new java.awt.Color(232, 232, 232));
        jPanel6.setToolTipText("");
        jPanel6.setPreferredSize(new java.awt.Dimension(800, 50));
        jPanel6.setLayout(new java.awt.BorderLayout());

        backBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/oviceTime/icon/Back_30px.png"))); // NOI18N
        backBtn.setBorder(null);
        backBtn.setBorderPainted(false);
        backBtn.setContentAreaFilled(false);
        backBtn.setPreferredSize(new java.awt.Dimension(50, 50));
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });
        jPanel6.add(backBtn, java.awt.BorderLayout.WEST);

        statuslabel.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        statuslabel.setForeground(new java.awt.Color(93, 153, 190));
        statuslabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        statuslabel.setText("No Record Found");
        statuslabel.setToolTipText("");
        statuslabel.setPreferredSize(new java.awt.Dimension(600, 50));
        jPanel6.add(statuslabel, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(232, 232, 232));
        jPanel1.setToolTipText("");
        jPanel1.setPreferredSize(new java.awt.Dimension(150, 50));
        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 0, 0));

        editBtn.setBackground(new java.awt.Color(47, 58, 74));
        editBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/edit_user.png"))); // NOI18N
        editBtn.setToolTipText("");
        editBtn.setBorder(null);
        editBtn.setBorderPainted(false);
        editBtn.setContentAreaFilled(false);
        editBtn.setOpaque(true);
        editBtn.setPreferredSize(new java.awt.Dimension(50, 50));
        editBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editBtnActionPerformed(evt);
            }
        });
        jPanel1.add(editBtn);

        mailbtn.setBackground(new java.awt.Color(47, 58, 74));
        mailbtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/gmail_log2.png"))); // NOI18N
        mailbtn.setBorder(null);
        mailbtn.setBorderPainted(false);
        mailbtn.setContentAreaFilled(false);
        mailbtn.setOpaque(true);
        mailbtn.setPreferredSize(new java.awt.Dimension(50, 50));
        mailbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mailbtnActionPerformed(evt);
            }
        });
        jPanel1.add(mailbtn);

        deleteBtn.setBackground(new java.awt.Color(47, 58, 74));
        deleteBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/delete.png"))); // NOI18N
        deleteBtn.setBorder(null);
        deleteBtn.setBorderPainted(false);
        deleteBtn.setContentAreaFilled(false);
        deleteBtn.setOpaque(true);
        deleteBtn.setPreferredSize(new java.awt.Dimension(50, 50));
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });
        jPanel1.add(deleteBtn);

        jPanel6.add(jPanel1, java.awt.BorderLayout.EAST);

        add(jPanel6, java.awt.BorderLayout.PAGE_START);

        cardsContainer.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        cardsContainer.setToolTipText("");
        cardsContainer.setPreferredSize(new java.awt.Dimension(800, 500));
        cardsContainer.setLayout(new java.awt.CardLayout());
        add(cardsContainer, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        // TODO add your handling code here:
        backBtn.setVisible(false);
        editBtn.setVisible(false);
        deleteBtn.setVisible(false);
        mailbtn.setVisible(false);
        switchCardsInHome(STAFF_LIST_PANEL);
    }//GEN-LAST:event_backBtnActionPerformed

    private void editBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editBtnActionPerformed
        // TODO add your handling code here:
        staffModel = dbHelper.getStaffDetails(fullname);
        window = new JFrame();
        JFXPanel jfxPanel = new JFXPanel();
       
            Platform.runLater(() -> {

            try { 
                StaffRegistrationController.staffModel = staffModel;
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/javafxbeta/StaffRegistration.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root, 800, 600);
                scene.getStylesheets().add(getClass().getResource("/css/test.css").toExternalForm());
                jfxPanel.setScene(scene);                

                SwingUtilities.invokeLater(() -> {
                    window.add(jfxPanel);                   
                    window.setUndecorated(true);                   
                    window.pack(); 
                     window.setLocationRelativeTo(null);
                    window.setVisible(true);
                    StaffRegistrationController.setOnUpdateListener(this);
                                      StaffRegistrationController.setOnFormClosedListener(this);
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        });      
    }//GEN-LAST:event_editBtnActionPerformed

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        // TODO add your handling code here:
        ConfirmDialog dialog = new ConfirmDialog(windowFrame, true);
        dialog.setMessge("Are you sure you want to delete \n" + fullname);
        dialog.setVisible(true);
        int userResponse = dialog.getReturnStatus();
        int result = -1;
        if(userResponse > 0){// if the reponse is OK == 1
           result = dbHelper.deleteStaff(fullname);
        }
        if(result > 0){//User account deleted
            AlertDialog dial = new AlertDialog(windowFrame, true);
            dial.setMessage("User account Successfully Deleted");
            dial.setVisible(true);
            //listener.onUpdateStaff(fullname);
            MainHomeForm.summaryPanel.refresh();
            backBtn.setVisible(false);
            editBtn.setVisible(false);
            deleteBtn.setVisible(false);
            mailbtn.setVisible(false);
            createStaffTable();
            switchCardsInHome(STAFF_LIST_PANEL);
        }
    }//GEN-LAST:event_deleteBtnActionPerformed

    private void mailbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mailbtnActionPerformed
        // TODO add your handling code here:
        //mailbtn.setEnabled(false);
        MailForm form = new MailForm();
        form.setEmailSentListener(this);
        form.setSenderEmail(staffEmail);
        form.setVisible(true);
    }//GEN-LAST:event_mailbtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    private javax.swing.JPanel cardsContainer;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JButton editBtn;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JButton mailbtn;
    private javax.swing.JLabel statuslabel;
    // End of variables declaration//GEN-END:variables

}
