/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxbeta;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.NumberValidator;
import com.jfoenix.validation.RequiredFieldValidator;
import static com.sun.org.apache.bcel.internal.util.SecuritySupport.getResourceAsStream;
import config.AppConfig;
import java.awt.image.RenderedImage;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.image.Image;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.StringConverter;
import javax.imageio.ImageIO;

import javax.swing.JFileChooser;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import oviceTime.MainHomeForm;
import oviceTime.database.DbHelper;
import oviceTime.dialogs.AlertDialog;
import oviceTime.model.AdminModel;
import oviceTime.model.StaffModel;
import oviceTime.listeners.OnFormClosedListener;
import oviceTime.listeners.OnStaffRegisteredListener;
import oviceTime.listeners.OnUpdateListener;

/**
 * FXML Controller class
 *
 * @author user
 */
public class StaffRegistrationController implements Initializable {

    @FXML
    private VBox leftpanel;
    @FXML
    private ImageView staffImage;
    @FXML
    private JFXButton uploadBtn;
    @FXML
    private Label personalDetails;
    @FXML
    private VBox personalDetailsPanel;
    @FXML
    private JFXRadioButton maleRadioBtn;
    @FXML
    private JFXRadioButton femaleRadioBtn;
    @FXML
    private JFXTextField staffFirstNameField;
    @FXML
    private JFXComboBox<String> bloodGrpCombo;
    @FXML
    private JFXTextField staffLastNameField;
    @FXML
    private JFXComboBox<String> jobDescriptionCombo;
    @FXML
    private JFXComboBox<String> academicQuaCombo;
    @FXML
    private HBox datePickerPanel;
    @FXML
    private JFXCheckBox adminCheckBos;
    @FXML
    private Separator separator;
    @FXML
    private VBox rightpanel;
    @FXML
    private Label contactInfo;
    @FXML
    private VBox contactInfoPanel;
    @FXML
    private JFXTextField homeAddress;
    @FXML
    private JFXTextField emailAddress;
    @FXML
    private JFXTextField staffPhnumberField;
    @FXML
    private Label GuardianDetails;
    @FXML
    private VBox guardianlDetailsPanel;
    @FXML
    private JFXTextField nokNameField;
    @FXML
    private JFXTextField nokAddressField;
    @FXML
    private JFXTextField nokEmailField;
    @FXML
    private JFXTextField nokPhoneNumberField;
    @FXML
    private JFXButton submitBtn;

     private static final String FX_LABEL_FLOAT_TRUE = "-fx-label-float:true;";
    private static final String EM1 = "1em";
    private static final String ERROR = "error";
    private RequiredFieldValidator validator;
    @FXML
    private ImageView closebtn;
    
    private boolean isGenderset = false, isAdmin = false;
    private String bloodgrp = "", firstName, lastName, jobDscrptn = "", acadQual = "", address, gender;
    private String email, phNumber, nokFullname, nokAddress, nokEmail, nokPhNumber, dob = "", password;
    private NumberValidator numberValidator;
    private JFXDatePicker datePickerFX;
    private String imageLocation;
    private File staffImageFile, file;
    
    private DbHelper dbHelper;
    @FXML
    private JFXPasswordField adminPasswordField;
    @FXML
    private JFXSnackbar snackBar;
    private Image image;
    
    public static OnStaffRegisteredListener listener;
    
    public static OnUpdateListener uListener;
    
    public static OnFormClosedListener formListener;
    public static StaffModel staffModel;
    private String[] bloodgrps;
    private String[] acdQual;
    private String[] jobDescriptn;
    private AdminModel adminModel;
    @FXML
    private BorderPane mainPanel;
    private JFXSnackbar snackbar;
    


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        dbHelper = new DbHelper(true);
        
        leftpanel.setStyle("-fx-background-color:WHITE;");
        rightpanel.setStyle("-fx-background-color:WHITE;");
        staffImage.setStyle("-fx-backgroung-color:RED ");//#F0F0F0
        
        snackbar = new JFXSnackbar(mainPanel);              
        
        setValidatorAndFloat(staffFirstNameField, "First name");
        setPasswordFieldValidator(adminPasswordField, "password");
        setValidatorAndFloat(staffLastNameField, "Last name");
        setValidatorAndFloat(homeAddress, "Home Address");
        setNumberFieldValidator(staffPhnumberField, "Phone number");
        setValidatorAndFloat(emailAddress, "Email Address");
        setValidatorAndFloat(nokNameField, "Full name ");
        setValidatorAndFloat(nokAddressField, "Address");
        setValidatorAndFloat(nokEmailField, "Email ");
        setNumberFieldValidator(nokPhoneNumberField, "Mobile number");
        
        submitBtn.getStyleClass().add("button-raised");
        
        datePickerFX = new JFXDatePicker();
        datePickerFX.setPromptText("Date of Birth");
        datePickerPanel.getChildren().add(datePickerFX); 
        
        
        bloodgrps = new String[]{" O+ (O positive)", " O- (O negative)", "AB", "A- / B-"};
        loadGroupCombo(bloodGrpCombo, bloodgrps);
        
        acdQual = new String[]{"Phd ", "MSc ", "BSc/ BEng", "Others"};
        loadGroupCombo(academicQuaCombo, acdQual);
        
        jobDescriptn = new String[]{"General Manager", "Manager", "Full-time Staff",
                                            "Contract Staff", "Intern", "others"};
        loadGroupCombo(jobDescriptionCombo, jobDescriptn);
        setStaffDetails();
    }    
    
    /**
     * sets the validator and floating property for each textfield
     * @param tf 
     */
    private void setValidatorAndFloat(JFXTextField tf, String id){
        
        tf.setLabelFloat(true);        
        tf.setStyle(FX_LABEL_FLOAT_TRUE);       
        
        
        validator = new RequiredFieldValidator();
        validator.setMessage(id + " Required");
        tf.getValidators().add(validator);        
        tf.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                tf.validate();
            }
           
        });
    }
    
  
    
    /**
     * sets the validator and floating property for each textfield
     * @param tf 
     */
    private void setNumberFieldValidator(JFXTextField tf, String id){
        
        tf.setLabelFloat(true);        
        tf.setStyle(FX_LABEL_FLOAT_TRUE);      

        numberValidator = new NumberValidator();
        numberValidator.setMessage(id + " Required");
        tf.getValidators().add(numberValidator);        
        tf.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                tf.validate();
            }
           
        });
    }
    
    /**
     * sets the validator and floating property for password textfield
     * @param tf 
     */
    private void setPasswordFieldValidator(JFXPasswordField tf, String id){
        
        tf.setLabelFloat(true);        
        tf.setStyle(FX_LABEL_FLOAT_TRUE);      

        validator = new RequiredFieldValidator();
        validator.setMessage(id + " Required");
        tf.getValidators().add(validator);        
        tf.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                tf.validate();
            }
           
        });
    }
    
    /**
     * set the listener to notify detailspanel when an update has been made 
     * to staff's record
     * @param listener 
     */
    public static void setOnUpdateListener(OnUpdateListener listener){
        uListener = listener;
    }
    
       
    /**
     * load the items of group combo
     * @param cb
     * @param items 
     */
    private void loadGroupCombo(JFXComboBox cb, String[] items){
        
        for(int i = 0; i < items.length; i++){
            cb.getItems().add(items[i].toString());
        }        
        cb.setEditable(true);

    }
    
    /**
     * set the listener for staff registered notification
     * @param listener 
     */
    public static void setOnStaffRegisteredlistener(OnStaffRegisteredListener lst){
        listener = lst;
    }
    
     /**
     * set the listener for close the registration form
     * @param listener 
     */
    public static void setOnFormClosedListener(OnFormClosedListener lst){
        formListener = lst;
    }

    @FXML
    private void femaleClicked(MouseEvent event) {
        maleRadioBtn.setSelected(false);
        gender = "Mrs";
    }

    @FXML
    private void uploadBtnClicked(MouseEvent event) {
        
       
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Choose image");       
        chooser.getExtensionFilters().addAll(
               // new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("Jpeg only", "*.jpg")
        );
                      
         file = chooser.showOpenDialog(new Stage());
         if(file != null){
            image = new Image(file.toURI().toString());
            
            staffImage.setImage(image);
         }
         
    }

    @FXML
    private void maleClicked(MouseEvent event) {
        femaleRadioBtn.setSelected(false);
        gender = "Mr";
    }

    @FXML
    private void adminPriviledgeClicked(MouseEvent event) {
       
        // check if the password field is visible
        if(adminPasswordField.isVisible()){
            isAdmin = false;
            adminPasswordField.setVisible(false);

        }
        else{ 
             isAdmin = true;
             adminPasswordField.setVisible(true);
        }
       
    }
    
      /**
     * set up the details to edit 
     */
    private void setStaffDetails(){
        if(staffModel != null){
            String fullname = staffModel.getStaffFullname();
            String[] splitName = fullname.split(" ");
            gender = splitName[0];
            String firstname = splitName[1];
            String lastName = splitName[2];
            
            if(gender.equals("Mr")){
                maleRadioBtn.setSelected(true);
            }else{
                femaleRadioBtn.setSelected(true);
            }
            
            staffFirstNameField.setText(firstname);
            staffLastNameField.setText(lastName);
            homeAddress.setText(staffModel.getStaffAddress());
            emailAddress.setText(staffModel.getStaffEmail());
            staffPhnumberField.setText(staffModel.getStaffPhNumber());
            nokNameField.setText(staffModel.getNokFullname());
            nokAddressField.setText(staffModel.getNokAddress());
            
            try{
                nokEmailField.setText(staffModel.getNokEmail());
                nokPhoneNumberField.setText(staffModel.getNokPhnumber());
                bloodGrpCombo.setValue(staffModel.getBloodGrp());
                jobDescriptionCombo.setValue(staffModel.getJobDescrptn());
                academicQuaCombo.setValue(staffModel.getAcadQual());   
            }catch(NullPointerException e){
                
            }                  
            datePickerFX.setValue(LocalDate.parse(staffModel.getdOfBirth()));            
            String url = staffModel.getImagepath();            
            submitBtn.setText("Update");
        
            
            if(url != null && !url.isEmpty()){
                staffImageFile = new File(url);
                Image image = new Image(staffImageFile.toURI().toString());                
                staffImage.setImage(image);
            }
            
            
            adminModel = dbHelper.getAdminWithId(staffModel.getStaffId());
            if(adminModel != null){
                adminCheckBos.setSelected(true);
                
                isAdmin = true;
                adminPasswordField.setText(adminModel.getPassword());
                adminPasswordField.setVisible(true);
            }
            
        }
    }

    @FXML
    private void submitBtnClicked(MouseEvent event) {
        
       firstName = staffFirstNameField.getText();
       lastName =  staffLastNameField.getText();
       String fullname = gender + " " + firstName + " " + lastName;
       address =   homeAddress.getText();
       email = emailAddress.getText();
       phNumber = staffPhnumberField.getText();
       nokFullname = nokNameField.getText();
       nokAddress = nokAddressField.getText();
       nokEmail = nokEmailField.getText();
       nokPhNumber = nokPhoneNumberField.getText();
       password = adminPasswordField.getText();
       
       try{
//       if(datePickerFX.getValue()!= null && bloodGrpCombo.getValue() != null && jobDescriptionCombo.getValue() != null &&
//              academicQuaCombo.getValue() != null ){
                LocalDate local = datePickerFX.getValue();
                dob = local.toString();
                bloodgrp = bloodGrpCombo.getValue();       
                jobDscrptn = jobDescriptionCombo.getValue();
                acadQual = academicQuaCombo.getValue();
     //  }
       }catch(NullPointerException e){}
       
      
       
       if(!fullname.isEmpty() && !address.isEmpty() && !email.isEmpty() && !phNumber.isEmpty() && !nokFullname.isEmpty()
               && !nokAddress.isEmpty() && !dob.isEmpty() && !gender.isEmpty()){
            String fingerPrint = MainHomeForm.fingerstring;
            StaffModel model = new StaffModel(fullname, address, phNumber, email, fingerPrint);
            model.setBloodGrp(bloodgrp);
            model.setJobDescrptn(jobDscrptn);
            model.setAcadQual(acadQual);
            model.setdOfBirth(dob);
            model.setNokFullname(nokFullname);
            model.setNokAddress(nokAddress);
            model.setNokEmail(nokEmail);
            model.setNokPhnumber(nokPhNumber);        
            
            
            //if its registration the txt will be submit
            //else it will be update
            if(submitBtn.getText().equalsIgnoreCase("submit")){
                
                if(file != null){// if the image is uploaded
                    saveStaffImage(image, fullname);
                    model.setImagepath(imageLocation);
                }
                
                int result = dbHelper.addStaff(model);
                if(result > 0){
                    snackbar.show("Staff data Saved", 3000);                    

                    if(isAdmin){
                        AdminModel adminModel = new AdminModel(result, password, fingerPrint);
                        dbHelper.addAdmin(adminModel);
                    }
                }
                else{
                    snackbar.show("Error inserting data", 3000);
                                
                }
            
                listener.OnStaffREgistered(fullname);
            }
            else{//update staff details
                     if(file != null){  
                         
                        saveStaffImage(image, fullname);
                        model.setImagepath(imageLocation);
                    }
                     else{
                         model.setImagepath(staffModel.getImagepath());
                     }
                    model.setStaffId(staffModel.getStaffId());
                    
                    
                    int result = dbHelper.updateStaff(model);
                    if(result > 0){
                       snackbar.show("Data Updated", 3000);        
                        
                        //Add, update or delete admin, when conditions are satisfied
                        AdminModel adModel = new AdminModel(staffModel.getStaffId(), password, fingerPrint);
                        if(adminModel != null && isAdmin){//admin exist, so update the details                           
                            
                            dbHelper.updateAdmin(adModel);
                        }else if(adminModel != null && !isAdmin){//if the staff was an admin but it has been removed 
                            dbHelper.deleteAdmin(staffModel.getStaffId());
                        }
                        if(adminModel == null && isAdmin){//wasnt an admin before but has a new admin priviledge, then insert
                            dbHelper.addAdmin(adModel);
                        }
                    }
                    else{
                       snackbar.show("Error updating staff's data ", 3000);                      
                    }

                    uListener.onUpdateStaff(fullname);
            }
            
       }
       else{
             snackbar.show("Some Essential fields Are required", 3000);                  
       }  

    }
    
    /**
     * save the image into appdata with the staff's full name
     * @param image
     * @param fullname 
     */
    public void saveStaffImage(Image image, String fullname){
        try {
             if(image != null){
                imageLocation = AppConfig.APP_DATA_LOCATION_OJU + fullname + ".jpg";  

                  File file = new File(imageLocation);
                  ImageIO.write(SwingFXUtils.fromFXImage(image, null), "JPG", file);
                  System.out.println("Image saved in " + file.getAbsolutePath());
                   
             }
            } catch (IOException e1) {
                e1.printStackTrace();
        }
        
    }

    @FXML
    private void closebtnMouseExited(MouseEvent event) {
    }

    @FXML
    private void closeBtnMouseOver(MouseEvent event) {
    }

    @FXML
    private void closeBtnClicked(MouseEvent event) {
       formListener.onFormClosed();
    }
    
    @FXML
    private void onStaffEmailMouseExited(MouseEvent event) {
        String text = emailAddress.getText();
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean b = text.matches(EMAIL_REGEX);        
           
        if(b == false){ 
            emailAddress.setStyle(" -fx-text-fill: RED;");
            snackbar.show("Please enter a valid email address", 2000);               
        } 
        else{
             emailAddress.setStyle(" -fx-text-fill: BLACK;");
        }  
    }

    @FXML
    private void onNokEmailMouseExited(MouseEvent event) {
         String text = nokEmailField.getText();
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        Boolean b = text.matches(EMAIL_REGEX);        
           
        
        if(b == false){ 
            nokEmailField.setStyle(" -fx-text-fill: RED;");
            snackbar.show("Please enter a valid email address", 2000);               
        } 
        else{
             nokEmailField.setStyle(" -fx-text-fill: BLACK;");
        }
    }

    @FXML
    private void staffEmailTextChanged(InputMethodEvent event) {
    }

    @FXML
    private void nokEmailTextChanged(InputMethodEvent event) {
    }
    
    
    
}
