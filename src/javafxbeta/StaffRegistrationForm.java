/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxbeta;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author user
 */
public class StaffRegistrationForm extends Application {
    
    public static String fingerPrint;
    static Stage stage;
    
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("StaffRegistration.fxml"));
       
        Scene scene = new Scene(root);
         scene.getStylesheets()
            .add(getClass().getResource("/css/test.css").toExternalForm());
        stage.setScene(scene);
        
        this.stage = stage;
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public static String getFingerPrint() {
        return fingerPrint;
    }

    public static void setFingerPrint(String fingerPrint) {
        StaffRegistrationForm.fingerPrint = fingerPrint;
    }
    
  
    
    
}
